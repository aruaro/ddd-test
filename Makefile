# defines variables

PHP=$(shell command -v php 2> /dev/null)
COMPOSER=$(shell command -v composer 2> /dev/null)
NPM=$(shell command -v npm 2> /dev/null)

SERVER_ADDR = 127.0.0.1:8000
PUBLIC_DIR = public
TMP_DIR = var

sources = config public src templates .env
modules = src tests
build_name = artifact
build_dir = build/$(build_name)

# include $(envars)
# export $(shell sed 's/=.*//' $(envars))

.PHONY: list
list:
	@echo $(TMP_DIR)
	@echo ""
	@echo "Useful targets:"
	@echo ""
	@echo "  test         = run tests"
	@echo "  test_unit    = run unit tests"
	@echo "  test_http    = run http tests"
	@echo "  code_check   = lint and analyse code"
	@echo ""
	@echo "  assets       = run webpack"
	@echo ""
	@echo "  php_deps     = install composer dependencies"
	@echo "  node_deps    = install node dependencies"
	@echo ""
	@echo "  clean        = clean: vendor, node_modules, var/cache, var/logs"
	@echo "  clean_tmp    = clean var/cache, var/logs"
	@echo "  clean_deps   = remove vendor and node_modules"
	@echo ""
	@echo "  server_dev   = start dev server"
	@echo ""
	@echo "  db_install   = install DB migrations"
	@echo "  db_uninstall = rollback DB migrations"
	@echo "  db_reset     = reset DB migrations"
	@echo "  db_reset     = seed DB"
	@echo ""
	@echo "  build        = build deploy artifact"

.PHONY: test
test: php_deps test_unit test_http

.PHONY: code_check
code_check: php_deps vendor/bin/phpcs vendor/bin/phpstan
	vendor/bin/phpcs
	vendor/bin/phpstan analyse

.PHONY: php_deps
php_deps: vendor/autoload.php

vendor/autoload.php: composer.json
	$(COMPOSER) install
	touch $@

.PHONY: test_unit
test_unit: vendor/bin/phpunit
	vendor/bin/phpunit --testsuite unit

.PHONY: test_http
test_http: bin/console vendor/bin/phpunit
	rm database/data.db || true
	vendor/bin/phpunit --bootstrap ./tests/bootstrap.php --testsuite http

.PHONY: node_deps
node_deps: package.json
	$(NPM) install
	touch $@

.PHONY: clean
clean: clean_tmp clean_deps

.PHONY: clean_tmp
clean_tmp: server_stop
	test ! -e "$(TMP_DIR)/cache" || rm -r "$(TMP_DIR)/cache"
	test ! -e "$(TMP_DIR)/log" || rm -r "$(TMP_DIR)/log"
	test ! -e "$(TMP_DIR)/sessions" || rm -r "$(TMP_DIR)/sessions"

.PHONY: clean_deps
clean_vendor:
	test ! -e vendor || rm -r vendor
	test ! -e node_modules || rm -r node_modules

.PHONY: server_dev
server_dev:
	$(PHP) -S $(SERVER_ADDR) -t $(PUBLIC_DIR)

.PHONY: db_install
db_install: vendor/bin/doctrine-migrations
	bin/console doctrine:migrations:migrate latest --no-interaction --quiet --no-debug

.PHONY: db_uninstall
db_uninstall: vendor/bin/doctrine-migrations
	bin/console doctrine:migrations:migrate first --no-interaction --quiet --no-debug

.PHONY: db_reset
db_reset: vendor/bin/doctrine-migrations db_uninstall db_install

.PHONY: db_check
db_check: vendor/bin/doctrine-migrations
	bin/console doctrine:migrations:migrate latest --dry-run

.PHONY: db_seed
db_seed: bin/console
	bin/console app:seed-db --no-debug

build: build/$(build_name).tar.gz

build/$(build_name).tar.gz: $(sources) vendor
	-rm -rf $(build_dir)
	mkdir -p $(build_dir)
	mkdir -p $(build_dir)/var/cache
	cp -RP $(sources) composer.* $(build_dir)
	cp -RP bin/console $(build_dir)/bin
	composer install --working-dir=$(build_dir) --no-dev --optimize-autoloader --no-plugins --no-scripts
	tar -czf $@ -C $(build_dir) .
	-rm -rf $(build_dir)

