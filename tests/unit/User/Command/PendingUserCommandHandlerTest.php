<?php

namespace Foobar\Tests\Unit\User\Command;

use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Foobar\Common\ValueObject\Uuid;
use Foobar\Tests\Unit\User\UserDataTrait;
use Foobar\User\Command\CreatePendingUser;
use Foobar\User\Command\Handler\PendingUserCommandHandler;
use Foobar\User\Command\RemovePendingUser;
use Foobar\User\Entity\PendingUser;
use Foobar\User\Query\LoadPendingUserCollection;
use Foobar\User\Query\LoadPendingUserState;
use Foobar\User\Repository\PendingUserRepository;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * @group User
 */
class PendingUserCommandHandlerTest extends TestCase
{
    use UserDataTrait;

    /** @var PendingUserRepository|MockObject */
    private $repository;
    /** @var UserPasswordEncoderInterface|MockObject */
    private $passwordEncoder;
    /** @var PendingUserCommandHandler */
    private $handler;

    public function setUp()
    {
        $this->repository      = $this->createMock(PendingUserRepository::class);
        $this->passwordEncoder = $this->createMock(UserPasswordEncoderInterface::class);

        $this->passwordEncoder->expects($this->any())->method('encodePassword')
            ->willReturn('password');

        $this->handler = new PendingUserCommandHandler($this->repository, $this->passwordEncoder);

        $this->handler->setEventDispatcher(
            $this->createMock(EventDispatcherInterface::class)
        );
    }

    public function testHandleCreatePendingUser()
    {
        $command = new CreatePendingUser('email', 'password');

        $this->repository->expects($this->once())->method('create')
            ->with($this->callback(function ($pendingUser) {
                return $pendingUser->getEmail() === 'email'
                    && $pendingUser->getPassword() == 'password';
            }));

        $this->handler->handleCreatePendingUser($command);
    }

    public function testHandleRemovePendingUser()
    {
        $command = RemovePendingUser::byEmail('email');

        $this->repository->expects($this->once())->method('findByEmail')
            ->willReturn($this->getPendingUser());

        $this->repository->expects($this->once())->method('delete')
            ->with($this->callback(function ($pendingUser) {
                return $pendingUser->getEmail() === 'email';
            }));

        $this->handler->handleRemovePendingUser($command);
    }
}
