<?php

namespace Foobar\Tests\Unit\User\Command\Handler;

use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Foobar\Tests\Unit\User\UserDataTrait;
use Foobar\User\Command\ActivateUser;
use Foobar\User\Command\CreateUser;
use Foobar\User\Command\DeactivateUser;
use Foobar\User\Command\Handler\UserCommandHandler;
use Foobar\User\Command\RemoveUser;
use Foobar\User\Command\UpdateUser;
use Foobar\User\Repository\UserRepository;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * @group User
 */
class UserCommandHandlerTest extends TestCase
{
    use UserDataTrait;

    /** @var UserRepository|MockObject */
    private $repository;
    /** @var UserPasswordEncoderInterface|MockObject */
    private $passwordEncoder;
    /** @var UserCommandHandler */
    private $handler;

    public function setUp()
    {
        $this->repository      = $this->createMock(UserRepository::class);
        $this->passwordEncoder = $this->createMock(UserPasswordEncoderInterface::class);

        $this->passwordEncoder->expects($this->any())->method('encodePassword')
            ->willReturn('password');

        $this->handler = new UserCommandHandler($this->repository, $this->passwordEncoder);

        $this->handler->setEventDispatcher(
            $this->createMock(EventDispatcherInterface::class)
        );
    }

    public function testHandleCreateUser()
    {
        $command           = new CreateUser();
        $command->id       = 'id';
        $command->email    = 'email';
        $command->password = 'password';
        $command->name     = 'name';
        $command->address  = [
            'street'   => 'street',
            'city'     => 'city',
            'locality' => 'locality',
            'country'  => 'country',
            'zipcode'  => 'zipcode',
        ];

        $this->repository->expects($this->once())->method('create')
            ->with($this->callback(function ($user) {
                return $user->getId() === 'id';
            }));

        $this->handler->handleCreateUser($command);
    }

    public function testHandleUpdateUser()
    {
        $user    = $this->getUser();
        $command = UpdateUser::fromState($user->getState());

        $command->name = 'new name';

        $this->repository->expects($this->once())->method('findById')
            ->with($user->getId())
            ->willReturn($user);

        $this->repository->expects($this->once())->method('update')
            ->with($user);

        $this->handler->handleUpdateUser($command);

        $this->assertEquals('new name', $user->getProfile()->getName());
    }

    public function testHandleRemoveUser()
    {
        $user    = $this->getUser();
        $command = RemoveUser::byId($user->getId());

        $this->repository->expects($this->once())->method('findById')
            ->with($user->getId())
            ->willReturn($user);

        $this->repository->expects($this->once())->method('delete')
            ->with($user);

        $this->handler->handleRemoveUser($command);
    }

    public function testHandleActivateUser()
    {
        $this->markTestIncomplete();
        return;

        $user    = $this->getUser();
        $command = ActivateUser::byId($user->getId());

        $this->repository->expects($this->once())->method('findById')
            ->with($user->getId())
            ->willReturn($user);

        $this->repository->method('update')
            ->with($user);

        $this->handler->handleActivateUser($command);
    }

    public function testHandleDeactivateUser()
    {
        $this->markTestIncomplete();
        return;

        $user    = $this->getUser();
        $command = DeactivateUser::byId($user->getId());

        $this->repository->expects($this->once())->method('findById')
            ->with($user->getId())
            ->willReturn($user);

        $this->repository->method('update')
            ->with($user);

        $this->handler->handleDeactivateUser($command);
    }
}
