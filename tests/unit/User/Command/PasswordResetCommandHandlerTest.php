<?php

namespace Foobar\Tests\Unit\User\Command;

use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Foobar\Common\ValueObject\Uuid;
use Foobar\User\Command\CheckPasswordReset;
use Foobar\User\Command\CreatePasswordReset;
use Foobar\User\Command\Handler\PasswordResetCommandHandler;
use Foobar\User\Entity\PasswordReset;
use Foobar\User\Repository\PasswordResetRepository;
use Foobar\User\ValueObject\SecureHash;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

/**
 * @group Application
 */
class PasswordResetCommandHandlerTest extends TestCase
{
    /** @var PasswordResetRepository|MockObject */
    private $repository;
    /** @var SessionInterface|MockObject */
    private $session;
    /** @var PasswordResetCommandHandler */
    private $handler;

    public function setUp()
    {
        $this->repository = $this->createMock(PasswordResetRepository::class);
        $this->session    = $this->createMock(SessionInterface::class);

        $this->handler = new PasswordResetCommandHandler(
            $this->repository,
            $this->session
        );

        $this->handler->setEventDispatcher(
            $this->createMock(EventDispatcherInterface::class)
        );
    }

    public function testHandleCreatePasswordReset()
    {
        $command        = new CreatePasswordReset();
        $command->email = 'test@email.com';

        $passwordReset = PasswordReset::create('userId');

        $this->repository->expects($this->once())->method('findByEmail')
            ->willReturn($passwordReset);

        $this->repository->expects($this->once())->method('create')
            ->with($this->callback(function (PasswordReset $passwordReset) {
                return $passwordReset->getUserId() == 'userId';
            }));

        $this->handler->handleCreatePasswordReset($command);
    }

    public function testHandleCheckPasswordReset()
    {
        $passwordReset = PasswordReset::fromState($this->getPasswordReset());
        $command       = CheckPasswordReset::byResetHash($passwordReset->getResetHash());

        $this->repository->expects($this->once())->method('findByResetHash')
            ->willReturn($passwordReset);

        $this->repository->expects($this->once())->method('update')
            ->with($this->callback(function (PasswordReset $passwordReset) {
                return $passwordReset->isChecked() === true;
            }));

        $this->handler->handleCheckPasswordReset($command);
    }

    private function getPasswordReset()
    {
        return [
            'user_id'    => (new Uuid())->getUuid(),
            'reset_hash' => SecureHash::generate()->getHash(),
            'checked'    => false,
            'expires_at' => (new \DateTimeImmutable())->format('Y-m-d h:i:s'),
        ];
    }
}
