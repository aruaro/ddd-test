<?php

namespace Foobar\Tests\Unit\User\Command;

use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Foobar\Common\ValueObject\Uuid;
use Foobar\Tests\Unit\User\UserDataTrait;
use Foobar\User\Command\CreatePhoto;
use Foobar\User\Command\Handler\PhotoCommandHandler;
use Foobar\User\Command\RemovePhoto;
use Foobar\User\Entity\Photo;
use Foobar\User\Query\LoadPhotoCollection;
use Foobar\User\Query\LoadPhotoState;
use Foobar\User\Repository\PhotoRepository;

/**
 * @group User
 */
class PhotoCommandHandlerTest extends TestCase
{
    use UserDataTrait;

    /** @var PhotoRepository|MockObject */
    private $repository;
    /** @var PhotoCommandHandler */
    private $handler;

    public function setUp()
    {
        $this->repository = $this->createMock(PhotoRepository::class);
        $this->handler    = new PhotoCommandHandler($this->repository);
    }

    public function testHandleCreatePhoto()
    {
        $command = new CreatePhoto('userId', 'path', $main = false);

        $this->repository->expects($this->once())->method('create')
            ->with($this->callback(function ($photo) {
                return $photo->getUserId() === 'userId'
                    && $photo->getPath() == 'path';
            }));

        $this->handler->handleCreatePhoto($command);
    }

    public function testHandleRemovePhoto()
    {
        $command = new RemovePhoto('userId', 'path');

        $this->repository->expects($this->once())->method('findById')
            ->willReturn($this->getPhoto());

        $this->repository->expects($this->once())->method('delete')
            ->with($this->callback(function ($photo) {
                return $photo->getUserId() === 'userId'
                    && $photo->getPath() == 'path';
            }));

        $this->handler->handleRemovePhoto($command);
    }
}
