<?php

namespace Foobar\Tests\Unit\User\Entity;

use PHPUnit\Framework\TestCase;
use Foobar\Tests\Unit\User\UserDataTrait;
use Foobar\User\Entity\PendingUser;

/**
 * @group User
 */
class PendingUserTest extends TestCase
{
    use UserDataTrait;

    public function testShouldGenerateTokenOnCreation()
    {
        $pendingUser = PendingUser::create('email', 'password');

        $this->assertNotEmpty($pendingUser->getToken());
    }

    public function testShouldExpireAt30DaysUponCreation()
    {
        $pendingUser  = PendingUser::create('email', 'password');
        $dateInterval = \DateInterval::createFromdateString('+30 days');

        $this->assertEquals(
            (new \DateTimeImmutable())->add($dateInterval)->format('Ymd'),
            $pendingUser->getExpiresAt()->format('Ymd')
        );
    }

    public function testCanBeCreatedFromState()
    {
        $pendingUser = PendingUser::fromState([
            'email'      => 'email',
            'password'   => 'password',
            'token'      => 'token',
            'checked'    => false,
            'expires_at' => '2030-01-01',
        ]);

        $this->assertEquals($pendingUser->getToken(), 'token');
        $this->assertEquals($pendingUser->getExpiresAt()->format('Y-m-d'), '2030-01-01');
    }

    public function testCanBeMarkedAsChecked()
    {
        $pendingUser = $this->getPendingUser();

        $this->assertFalse($pendingUser->isChecked());

        $pendingUser->markAsChecked();


        $this->assertTrue($pendingUser->isChecked());
    }
}
