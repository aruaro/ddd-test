<?php

namespace Foobar\Tests\Unit\User\Entity;

use PHPUnit\Framework\TestCase;
use Foobar\Common\Addressing\ValueObject\Address;
use Foobar\Tests\Unit\User\UserDataTrait;
use Foobar\User\Entity\User;
use Foobar\User\ValueObject\Flags;
use Foobar\User\ValueObject\Status;

/**
 * @group User
 */
class UserTest extends TestCase
{
    use UserDataTrait;

    public function testShouldSetDefaultValuesOnCreation()
    {
        $user = User::create('id', 'email', 'password');

        $this->assertInstanceOf(User::class, $user);
        $this->assertEquals('email', $user->getEmail());
    }

    public function testCanBeCreatedFromState()
    {
        $this->markTestIncomplete();

        $state = [
            'id'         => 'userId',
            'email'      => 'email',
            'password'   => 'password',
            'status'     => 'inactive',
            'flags'      => '{}',
            'name'       => 'name',
            'street'     => 'street',
            'city'       => 'city',
            'locality'   => 'locality',
            'country'    => 'country',
            'zipcode'    => 'zipcode',
            'created_at' => '2019-01-01',
            'updated_at' => '2019-01-01'
        ];

        $user = User::fromState($state);
    }

    public function testCanGetState()
    {
        $this->markTestIncomplete();

        $user = $this->getUser();
    }

    public function testCanUpdateProfile()
    {
        $this->markTestIncomplete();

        $user = $this->getUser();

        $user->changeEmail('email');

        $this->assertEquals('email', $user->getEmail());
    }

    public function testCanChangePassword()
    {
        $user = $this->getUser();

        $user->changePassword('password');

        $this->assertEquals('password', $user->getPassword());
    }

    public function testCanChangePhoto()
    {
        $this->markTestIncomplete();

        $user = $this->getUser();
    }

    public function testCanBeActivated()
    {
        $this->markTestIncomplete();

        $user = $this->getUser();
    }

    public function testCanBeDeactivated()
    {
        $this->markTestIncomplete();

        $user = $this->getUser();
    }
}
