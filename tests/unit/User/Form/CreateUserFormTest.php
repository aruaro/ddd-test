<?php

namespace Foobar\Tests\Unit\User\Form;

use PHPUnit\Framework\MockObject\MockObject;
use Foobar\Tests\Unit\FormValidationTrait;
use Foobar\Tests\Unit\User\UserDataTrait;
use Foobar\User\Command\CreateUser;
use Foobar\User\Exception\UserNotFound;
use Foobar\User\Form\CreateUserForm;
use Foobar\User\Repository\UserRepository;
use Symfony\Component\Form\Test\TypeTestCase;

/**
 * @group User
 * @group Form
 */
class CreateUserFormTest extends TypeTestCase
{
    use UserDataTrait;
    use FormValidationTrait;

    /** @var UserRepository|MockObject */
    private $repository;

    protected function setUp()
    {
        $this->repository = $this->createMock(UserRepository::class);

        parent::setUp();
    }

    protected function getPreloadedObjects() : array
    {
        return [new CreateUserForm($this->repository)];
    }

    public function formDataProvider()
    {
        $data = [
            'email'    => 'test@test.com',
            'password' => [
                'first'  => 'ABcd1234',
                'second' => 'ABcd1234',
            ],
            'name'     => 'Test User',
            'address' => [
                'street'   => 'street',
                'city'     => 'city',
                'locality' => 'locality',
                'zipcode'  => 'zipcode',
                'country'  => 'PH',
            ],
            'photos' => [
                ['path' => 'photo1']
            ],
        ];

        $invalid = $data;

        $invalid['email']   = 'invalid_email';
        $invalid['address'] = ['street' => 'steet', 'city' => ''];

        return [
            'valid form'   => [$data, false, true],
            'invalid form' => [$invalid, true, false],
        ];
    }

    /**
     * @dataProvider formDataProvider
     */
    public function testOnSubmit(array $data, bool $isEmailTaken, bool $isFormValid)
    {
        $this->repository->expects($this->once())->method('findByEmail')
            ->will(
                $isEmailTaken
                ? $this->returnValue($this->getUser())
                : $this->throwException(new UserNotFound())
            );

        $form = $this->factory->create(CreateUserForm::class);

        $form->submit($data);

        $this->assertInstanceOf(CreateUser::class, $form->getData());
        $this->assertEquals($isFormValid, $form->isValid());

        if ($isFormValid) {
            $this->assertEquals('test@test.com', $form->get('email')->getData());
        }
    }
}
