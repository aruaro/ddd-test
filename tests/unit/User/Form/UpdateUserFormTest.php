<?php

namespace Foobar\Tests\Unit\User\Form;

use PHPUnit\Framework\MockObject\MockObject;
use Foobar\Tests\Unit\FormValidationTrait;
use Foobar\Tests\Unit\User\UserDataTrait;
use Foobar\User\Command\UpdateUser;
use Foobar\User\Entity\User;
use Foobar\User\Exception\UserNotFound;
use Foobar\User\Form\UpdateUserForm;
use Foobar\User\Repository\UserRepository;
use Symfony\Component\Form\Test\TypeTestCase;

/**
 * @group User
 * @group Form
 */
class UpdateUserFormTest extends TypeTestCase
{
    use UserDataTrait;
    use FormValidationTrait;

    /** @var UserRepository|MockObject */
    private $repository;

    protected function setUp()
    {
        $this->repository = $this->createMock(UserRepository::class);

        parent::setUp();
    }

    protected function getPreloadedObjects() : array
    {
        return [new UpdateUserForm($this->repository)];
    }

    public function formDataProvider()
    {
        $data = [
            'email'    => 'test@test.com',
            'password' => [
                'first'  => 'ABcd1234',
                'second' => 'ABcd1234',
            ],
            'name'    => 'User2',
            'address' => [
                'street'   => 'street',
                'city'     => 'city',
                'locality' => 'locality',
                'zipcode'  => 'zipcode',
                'country'  => 'PH',
            ],
            'photos' => [
                ['path' => 'photo1']
            ],
        ];

        $invalid = $data;

        $invalid['email']   = 'invalid_email';
        $invalid['address'] = ['street' => 'steet', 'city' => ''];

        return [
            'valid form'   => [$data, false, true],
            'invalid form' => [$invalid, true, false],
        ];
    }


    /**
     * @dataProvider formDataProvider
     */
    public function testOnSubmit(array $data, bool $isEmailTaken, bool $isFormValid)
    {
        $this->repository->expects($this->once())->method('findByEmail')
            ->will(
                $isEmailTaken
                ? $this->returnValue($this->getUser())
                : $this->throwException(new UserNotFound())
            );

        $initial = UpdateUser::fromState($this->getUser()->getState());
        $form    = $this->factory->create(UpdateUserForm::class, $initial);

        $form->submit($data);

        $this->assertInstanceOf(UpdateUser::class, $form->getData());
        $this->assertEquals($isFormValid, $form->isValid());

        if ($isFormValid) {
            $this->assertEquals('User2', $form->get('name')->getData());
        }
    }
}
