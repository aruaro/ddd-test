<?php

namespace Foobar\Tests\Unit\User;

use Foobar\User\Entity\User;
use Foobar\User\Entity\PendingUser;
use Foobar\User\Entity\Photo;

trait UserDataTrait
{
    protected function getUser()
    {
        return User::fromState([
            'id'         => 'userId',
            'email'      => 'email',
            'password'   => 'password',
            'status'     => 'inactive',
            'flags'      => '{}',
            'name'       => 'name',
            'street'     => 'street',
            'city'       => 'city',
            'locality'   => 'locality',
            'country'    => 'country',
            'zipcode'    => 'zipcode',
            'created_at' => '2019-01-01',
            'updated_at' => '2019-01-01'
        ]);
    }

    protected function getPhoto()
    {
        return Photo::fromState([
            'user_id'  => 'userId',
            'mimetype' => 'mimetype',
            'path'     => 'path',
            'is_main'  => true
        ]);
    }

    protected function getPendingUser()
    {
        return PendingUser::fromState([
            'email'      => 'email',
            'password'   => 'password',
            'token'      => 'token',
            'checked'    => false,
            'expires_at' => '2030-01-01',
        ]);
    }
}
