<?php

namespace Foobar\Tests\Unit\User\Subscriber;

use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Foobar\Common\Mailer\Mailer;
use Foobar\Common\ValueObject\Uuid;
use Foobar\Tests\Unit\User\UserDataTrait;
use Foobar\User\Entity\PasswordReset;
use Foobar\User\Entity\User;
use Foobar\User\Event\PasswordResetChecked;
use Foobar\User\Event\PasswordResetCreated;
use Foobar\User\Repository\UserRepository;
use Foobar\User\Subscriber\PasswordResetMailerSubscriber;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\PostResponseEvent;
use Symfony\Component\HttpKernel\HttpKernelInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

/**
 * @group Common
 */
class PasswordResetMailerSubscriberTest extends TestCase
{
    use UserDataTrait;

    /** @var EventDispatcherInterface|MockObject */
    private $eventDispatcher;
    /** @var UrlGeneratorInterface|MockObject */
    private $urlGenerator;
    /** @var UserRepository|MockObject */
    private $userRepository;
    /** @var Mailer|MockObject */
    private $mailer;
    /** @var PasswordReset */
    private $passwordReset;
    /** @var PasswordResetMailerSubscriber */
    private $subscriber;

    public function setUp()
    {
        $this->passwordReset = PasswordReset::create('id');

        $this->eventDispatcher = $this->createMock(EventDispatcherInterface::class);
        $this->urlGenerator    = $this->createMock(UrlGeneratorInterface::class);
        $this->userRepository  = $this->createMock(UserRepository::class);
        $this->mailer          = $this->createMock(Mailer::class);

        $this->subscriber = new PasswordResetMailerSubscriber(
            $this->eventDispatcher,
            $this->urlGenerator,
            $this->userRepository,
            $this->mailer,
            'test@test.com'
        );
    }

    public function testOnPasswordResetCreated()
    {
        $this->eventDispatcher->expects($this->once())->method('addListener');

        $this->subscriber->onPasswordResetCreated(
            new PasswordResetCreated($this->passwordReset)
        );
    }

    public function testOnPasswordResetChecked()
    {
        $this->eventDispatcher->expects($this->once())->method('addListener');

        $this->subscriber->onPasswordResetChecked(
            new PasswordResetChecked($this->passwordReset)
        );
    }

    public function testOnKernelTerminate()
    {
        $this->testOnPasswordResetCreated();

        $this->userRepository->expects($this->once())->method('findById')
            ->with($this->passwordReset->getUserId())
            ->willReturn($this->getUser());

        $this->urlGenerator->expects($this->once())->method('generate')
            ->with(
                'app_password_reset_check',
                ['resetHash' => $this->passwordReset->getResetHash()]
            );

        $this->mailer->expects($this->once())->method('send')
            ->willReturn(true);

        $this->subscriber->onKernelTerminate(
            new PostResponseEvent(
                $this->createMock(HttpKernelInterface::class),
                $this->createMock(Request::class),
                $this->createMock(Response::class)
            )
        );
    }
}
