<?php

namespace Foobar\Tests\Unit\Security\Subscriber;

use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Foobar\Common\Security\SecurityUser as SecurityUser;
use Foobar\User\Entity\PasswordReset;
use Foobar\User\Entity\User;
use Foobar\User\Event\PasswordResetChecked;
use Foobar\User\Event\UserCreated;
use Foobar\User\Repository\UserRepository;
use Foobar\User\Subscriber\UserAutoLoginSubscriber;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\HttpKernelInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Core\Role\Role;

/**
 * @group Common
 */
class UserAutoLoginSubscriberTest extends TestCase
{
    /** @var UserRepository|MockObject */
    private $userRepository;
    /** @var TokenStorageInterface|MockObject */
    private $tokenStorage;
    /** @var UserAutoLoginSubscriber */
    private $subscriber;
    /** @var User */
    private $user;

    public function setUp()
    {
        $this->user = User::create('id', 'email', 'password');

        $this->userRepository = $this->createMock(UserRepository::class);
        $this->tokenStorage   = $this->createMock(TokenStorageInterface::class);

        $this->subscriber = new UserAutoLoginSubscriber($this->userRepository, $this->tokenStorage);
    }

    public function testOnUserCreated()
    {
        $this->prepareAutoLogin();

        $this->subscriber->onUserCreated(
            new UserCreated($this->user)
        );
    }

    public function testOnPasswordResetChecked()
    {
        $this->prepareAutoLogin();

        $this->userRepository->expects($this->once())->method('findById')
            ->with('id')
            ->willReturn($this->user);

        $this->subscriber->onPasswordResetChecked(
            new PasswordResetChecked(PasswordReset::create('id'))
        );
    }

    private function prepareAutoLogin()
    {
        $this->subscriber->onKernelRequest(
            new GetResponseEvent(
                $this->createMock(HttpKernelInterface::class),
                new Request([], [], ['_firewall_context' => 'context']),
                HttpKernelInterface::MASTER_REQUEST
            )
        );

        $role  = new Role('ROLE_USER');
        $user  = new SecurityUser('id', 'email', 'password', [$role->getRole()]);
        $token = new UsernamePasswordToken($user, null, 'context', [$role]);

        $this->tokenStorage->expects($this->once())->method('setToken')
            ->with($token);
    }
}
