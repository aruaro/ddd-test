<?php

namespace Foobar\Tests\Unit\User\Subscriber;

use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Foobar\Common\Mailer\Mailer;
use Foobar\Tests\Unit\User\UserDataTrait;
use Foobar\User\Entity\User;
use Foobar\User\Event\UserCreated;
use Foobar\User\Event\UserUpdated;
use Foobar\User\Subscriber\EmailConfirmationSubscriber;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\PostResponseEvent;
use Symfony\Component\HttpKernel\HttpKernelInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

/**
 * @group Common
 */
class EmailConfirmationSubscriberTest extends TestCase
{
    use UserDataTrait;

    /** @var EventDispatcherInterface|MockObject */
    private $eventDispatcher;
    /** @var UrlGeneratorInterface|MockObject */
    private $urlGenerator;
    /** @var Mailer|MockObject */
    private $mailer;
    /** @var EmailConfirmationSubscriber */
    private $subscriber;

    public function setUp()
    {
        $this->eventDispatcher = $this->createMock(EventDispatcherInterface::class);
        $this->urlGenerator    = $this->createMock(UrlGeneratorInterface::class);
        $this->mailer          = $this->createMock(Mailer::class);

        $this->subscriber = new EmailConfirmationSubscriber(
            $this->eventDispatcher,
            $this->urlGenerator,
            $this->mailer,
            'test@test.com'
        );
    }

    public function testShouldGetUserObjectOnUserCreated()
    {
        $this->eventDispatcher->expects($this->once())->method('addListener');

        $this->subscriber->onUserCreated(new UserCreated($this->getUser()));
    }

    public function testShouldGetUserObjectOnUserUpdated()
    {
        $this->eventDispatcher->expects($this->once())->method('addListener');

        $oldUser = $this->getUser();
        $newUser = $this->getUser();

        $this->subscriber->onUserUpdated(new UserUpdated($oldUser, $newUser));
    }

    public function testShouldSendEmailOnKernelTerminate()
    {
        $this->testShouldGetUserObjectOnUserCreated();

        $this->urlGenerator->expects($this->once())->method('generate')
            ->with(
                'app_user_activate',
                ['token' => 'token']
            );

        $this->mailer->expects($this->once())->method('send')
            ->willReturn(true);

        $this->subscriber->onKernelTerminate(
            new PostResponseEvent(
                $this->createMock(HttpKernelInterface::class),
                $this->createMock(Request::class),
                $this->createMock(Response::class)
            )
        );
    }
}
