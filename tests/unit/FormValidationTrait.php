<?php

namespace Foobar\Tests\Unit;

use Symfony\Component\Form\Extension\Validator\ValidatorExtension;
use Symfony\Component\Validator\Validation;
use Symfony\Component\Form\PreloadedExtension;

trait FormValidationTrait
{
    protected function getExtensions()
    {
        $validator = Validation::createValidatorBuilder()
            ->enableAnnotationMapping()
            ->getValidator();

        return [
            new ValidatorExtension($validator),
            new PreloadedExtension($this->getPreloadedObjects(), []),
        ];
    }

    protected function getPreloadedObjects() : array
    {
        return [];
    }
}
