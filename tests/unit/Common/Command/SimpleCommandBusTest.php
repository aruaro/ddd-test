<?php

namespace Foobar\Tests\Unit\Common\Command;

use PHPUnit\Framework\TestCase;
use Foobar\Common\Command\Exception\HandlerAlreadySubscribed;
use Foobar\Common\Command\Exception\HandlerNotFound;
use Foobar\Common\Command\SimpleCommandBus;

/**
 * @group Common
 */
class SimpleCommandBusTest extends TestCase
{
    public function testSubscribeThrowsHandlerAlreadySubscribed()
    {
        $this->expectException(HandlerAlreadySubscribed::class);

        $bus = new SimpleCommandBus();

        $bus->subscribe(new SimpleMockCommandHandler());
        $bus->subscribe(new SimpleMockCommandHandler());

        $returnValue = $bus->dispatch(new AnotherMockCommand());

        $this->assertEquals(AnotherMockCommandHandler::class, $returnValue);
    }

    public function testDispatchThrowsHandlerNotFound()
    {
        $this->expectException(HandlerNotFound::class);

        $bus = new SimpleCommandBus();
        $bus->dispatch(new MockCommand($triggerException = false));
    }

    public function testDispatchStopsWhenHandlerThrowsException()
    {
        $this->expectExceptionMessage(SimpleMockCommandHandler::EXCEPTION_MSG);

        $bus = new SimpleCommandBus();

        $bus->subscribe(new SimpleMockCommandHandler());
        $bus->dispatch(new MockCommand($triggerException = true));
    }

    public function testDispatchFindsHandlerInList()
    {
        $bus = new SimpleCommandBus();

        $bus->subscribe(new SimpleMockCommandHandler());
        $bus->subscribe(new AnotherMockCommandHandler());

        $returnValue = $bus->dispatch(new AnotherMockCommand());

        $this->assertEquals(AnotherMockCommandHandler::class, $returnValue);
    }
}
