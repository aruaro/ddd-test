<?php

namespace Foobar\Tests\Unit\Common\Command;

use Foobar\Common\Command\Command;

class MockCommand implements Command
{
    private $triggerException;

    public function __construct(bool $triggerException = false)
    {
        $this->triggerException = $triggerException;
    }

    public function triggerException() : bool
    {
        return $this->triggerException;
    }
}
