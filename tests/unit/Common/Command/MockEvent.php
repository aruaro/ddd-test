<?php

namespace Foobar\Tests\Unit\Common\Command;

use Symfony\Component\EventDispatcher\Event;

class MockEvent extends Event
{
    const NAME = 'test.event';
}
