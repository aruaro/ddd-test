<?php

namespace Foobar\Tests\Unit\Common\Command;

use Foobar\Common\Command\SimpleCommandHandler;

class AnotherMockCommandHandler extends SimpleCommandHandler
{
    public function handleAnotherMockCommand(AnotherMockCommand $command)
    {
        return get_class($this);
    }
}
