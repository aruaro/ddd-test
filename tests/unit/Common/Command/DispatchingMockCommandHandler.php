<?php

namespace Foobar\Tests\Unit\Common\Command;

use Symfony\Component\EventDispatcher\Event;
use Foobar\Common\Command\EventDispatchingCommandHandler;

class DispatchingMockCommandHandler extends EventDispatchingCommandHandler
{
    public function handleMockCommand(MockCommand $command)
    {
        $this->dispatchEvent(new MockEvent());

        return 'success';
    }
}
