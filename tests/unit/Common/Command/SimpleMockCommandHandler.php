<?php

namespace Foobar\Tests\Unit\Common\Command;

use Foobar\Common\Command\SimpleCommandHandler;

class SimpleMockCommandHandler extends SimpleCommandHandler
{
    const EXCEPTION_MSG = 'Mock command handler exception';

    public function handleMockCommand(MockCommand $command)
    {
        if ($command->triggerException()) {
            throw new \Exception(self::EXCEPTION_MSG);
        }
    }
}
