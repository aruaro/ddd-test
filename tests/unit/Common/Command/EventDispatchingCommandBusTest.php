<?php

namespace Foobar\Tests\Unit\Common\Command;

use PHPUnit\Framework\TestCase;
use Foobar\Common\Command\EventDispatchingCommandBus;
use Foobar\Common\Command\EventDispatchingCommandHandler;
use Foobar\Common\Command\Exception\HandlerNotFound;
use Foobar\Common\Command\SimpleCommandBus;
use Symfony\Component\EventDispatcher\Event;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * @group Common
 */
class EventDispatchingCommandBusTest extends TestCase
{
    /** @var EventDispatcherInterface */
    private $eventDispatcher;

    public function setUp()
    {
        $this->eventDispatcher = $this->createMock(EventDispatcherInterface::class);
    }

    public function testInstantiateSetsEventDispatcher()
    {
        $handler = $this->createMock(EventDispatchingCommandHandler::class);

        $handler->expects($this->once())->method('setEventDispatcher')
            ->with($this->eventDispatcher);

        $bus = new EventDispatchingCommandBus(new SimpleCommandBus([$handler]), $this->eventDispatcher);
    }

    public function testSubscribeSetsEventDispatcher()
    {
        $handler = $this->createMock(EventDispatchingCommandHandler::class);

        $handler->expects($this->once())->method('setEventDispatcher')
            ->with($this->eventDispatcher);

        $bus = new EventDispatchingCommandBus(new SimpleCommandBus(), $this->eventDispatcher);

        $bus->subscribe($handler);
    }

    public function testDispatchDispatchesEvent()
    {
        $handler = new DispatchingMockCommandHandler();

        $this->eventDispatcher->expects($this->once())->method('dispatch')
            ->with(MockEvent::NAME, $this->isInstanceOf(MockEvent::class));

        $bus = new EventDispatchingCommandBus(new SimpleCommandBus([$handler]), $this->eventDispatcher);

        $returnValue = $bus->dispatch(new MockCommand());

        $this->assertEquals('success', $returnValue);
    }
}
