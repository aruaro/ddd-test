<?php

namespace Foobar\Tests\Unit\Common\Addressing;

use GuzzleHttp\Client as GuzzleClient;
use GuzzleHttp\ClientInterface as GuzzleClientInterface;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response as GuzzleResponse;
use PHPUnit\Framework\TestCase;
use Foobar\Common\Addressing\Service\Mapbox;
use Foobar\Common\Addressing\ValueObject\Address;
use Foobar\Common\Addressing\ValueObject\Coordinate;

/**
 * @group Common
 */
class MapboxTest extends TestCase
{
    public function testFormsProperApiRequest()
    {
        $address = $this->getAddress();

        $place = urlencode($address->getPlaceName() . ' ' . $address->getLongFormat());
        $uri   = '/geocoding/v5/mapbox.places/' . $place . '.json';

        $guzzle = $this->createMock(GuzzleClientInterface::class);

        $guzzle->expects($this->once())->method('request')
            ->with($this->anything(), $uri, $this->anything())
            ->willReturn(new GuzzleResponse(200, [], ''));

        $mapbox = new Mapbox('apikey', $guzzle);

        $mapbox->geocodeAddress($address);
    }

    public function geocodeResponseProvider()
    {
        return [
            'returns geocode results' => [
                'latlng'   => [11.11, 12.12],
                'response' => new GuzzleResponse(200, [], json_encode([
                    'features' => [['center' => [11.11, 12.12],],],
                ])),
            ],
            'returns blank geocode on empty result' => [
                'latlng'   => [0, 0],
                'response' => new GuzzleResponse(200, [], json_encode([
                    'features' => []
                ])),
            ],
            'returns blank geocode on API error' => [
                'latlng'   => [0, 0],
                'response' => new GuzzleResponse(401, [], json_encode([
                    'message' => 'some error'
                ])),
            ],
        ];
    }

    /**
     * @dataProvider geocodeResponseProvider
     */
    public function testGeocodeLookup(array $latLng, GuzzleResponse $response)
    {
        $mapbox          = new Mapbox('apikey', $this->getGuzzleClient($response));
        $address         = $this->getAddress();
        $expectedAddress = $mapbox->geocodeAddress($address);

        $this->assertEquals($latLng[0], $expectedAddress->getCoordinate()->getLongitude());
        $this->assertEquals($latLng[1], $expectedAddress->getCoordinate()->getLatitude());
    }

    public function staticMapResponseProvider()
    {
        return [
            'returns base64 map image data' => [
                'expected' => base64_encode('mapimage'),
                'response' => new GuzzleResponse(200, [], 'mapimage'),
            ],
            'returns null on API error' => [
                'expected' => '',
                'response' => new GuzzleResponse(401, [], json_encode([
                    'message' => 'some error'
                ])),
            ],
        ];
    }

    /**
     * @dataProvider staticMapResponseProvider
     */
    public function testCreateStaticMap(string $expected, GuzzleResponse $response)
    {
        $mapbox     = new Mapbox('apikey', $this->getGuzzleClient($response));
        $coordinate = $this->getAddress()->getCoordinate();
        $staticMap  = $mapbox->createStaticMap($coordinate, Mapbox::DEFAULT_MARKER);

        $this->assertEquals($expected, $staticMap);
    }

    private function getGuzzleClient(GuzzleResponse $response)
    {
        $handler = new MockHandler([$response]);

        return new GuzzleClient([
            'handler' => HandlerStack::create($handler)
        ]);
    }

    private function getGeocodedAddress()
    {
        return Address::fromState([
            'street'    => 'street',
            'city'      => 'city',
            'locality'  => 'locality',
            'country'   => 'country',
            'zipcode'   => 'zipcode',
            'latitude'  => 22.22,
            'longitude' => 33.33,
        ]);
    }

    private function getAddress()
    {
        return Address::fromState([
            'street'   => 'street',
            'city'     => 'city',
            'locality' => 'locality',
            'country'  => 'country',
            'zipcode'  => 'zipcode',
        ]);
    }
}
