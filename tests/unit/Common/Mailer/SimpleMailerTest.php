<?php

namespace Foobar\Tests\Unit\Common\Mailer;

use Foobar\Common\Mailer\Adapter\MailerAdapter;
use Foobar\Common\Mailer\Exception\InvalidMessage;
use Foobar\Common\Mailer\Message\Message;
use Foobar\Common\Mailer\SimpleMailer;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

/**
 * @group Common
 */
class SimpleMailerTest extends TestCase
{
    /** @var MailerAdapter|MockObject */
    private $adapter;
    /** @var SimpleMailer */
    private $mailer;

    public function setUp()
    {
        $this->adapter = $this->createMock(MailerAdapter::class);

        $this->mailer = new SimpleMailer($this->adapter);
    }

    public function testSendWithSuccess()
    {
        $this->adapter->expects($this->once())->method('send')
            ->willReturn(true);

        $message = new Message('subject', 'body');
        $message->addSender('sender', 'sender@email.com');
        $message->addReceiver('receiver', 'receiver@email.com');

        $this->mailer->send($message);
    }

    public function incompleteMessageDataProvider()
    {
        return [
            [
                'case'      => 'no sender',
                'exception' => InvalidMessage::fromEmptySender()
            ],
            [
                'case'      => 'no receiver',
                'exception' => InvalidMessage::fromEmptyReceiver()
            ],
            [
                'case'      => 'no subject',
                'exception' => InvalidMessage::fromEmptySubject()
            ],
            [
                'case'      => 'no body',
                'exception' => InvalidMessage::fromEmptyBody()
            ],

        ];
    }

    /**
     * @dataProvider incompleteMessageDataProvider
     */
    public function testCannotSendIfIncompleteMessage(string $case, \Exception $exception)
    {
        $this->expectExceptionMessage($exception->getMessage());

        $subject = $case == 'no subject' ? '' : 'subject';
        $body    = $case == 'no body' ? '' : 'body';

        $message = new Message($subject, $body);

        if ($case != 'no sender') {
            $message->addSender('sender', 'sender@email.com');
        }

        if ($case != 'no receiver') {
            $message->addReceiver('receiver', 'receiver@email.com');
        }

        $this->mailer->send($message);
    }
}
