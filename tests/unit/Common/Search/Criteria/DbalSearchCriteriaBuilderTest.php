<?php

namespace Foobar\Tests\Unit\Common\Search\Criteria;

use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Query\Expression\ExpressionBuilder;
use Doctrine\DBAL\Query\QueryBuilder;
use PHPUnit\Framework\TestCase;
use Foobar\Common\Search\Criteria\DbalSearchCriteriaBuilder;
use Foobar\Common\Search\Criteria\SearchCriteria;
use Foobar\Common\Search\Exception\InvalidSearchCriteria;

/**
 * @group Common
 */
class DbalSearchCriteriaBuilderTest extends TestCase
{
    /** @var QueryBuilder */
    private $queryBuilder;
    /** @var DbalSearchCriteriaBuilder */
    private $criteriaBuilder;

    public function setUp()
    {
        $dbal = $this->getMockBuilder(Connection::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->queryBuilder    = new QueryBuilder($dbal);
        $this->criteriaBuilder = new DbalSearchCriteriaBuilder();
    }

    public function testBuildWithoutQueryBuilderExpectsException()
    {
        $this->expectExceptionMessage(
            InvalidSearchCriteria::fromNullQueryBuilder()->getMessage()
        );

        $this->criteriaBuilder->build();
    }

    public function testBuildWithUninitiatedQueryExpectsException()
    {
        $this->expectExceptionMessage('QueryBuilder has not been initiated');

        $this->criteriaBuilder->setQueryBuilder($this->queryBuilder);
        $this->criteriaBuilder->build();
    }

    public function testBuildWithSuccess()
    {
        $this->queryBuilder->select('*')->from('nothing');

        $criteria = $this->createMock(SearchCriteria::class);

        $criteria->expects($this->exactly(2))->method('getField')
            ->willReturn('field');

        $criteria->expects($this->exactly(2))->method('getValue')
            ->willReturn('test');

        $criteria->expects($this->once())->method('getExpression')
            ->willReturn('field = test');

        $this->criteriaBuilder->add($criteria);
        $this->criteriaBuilder->setQueryBuilder($this->queryBuilder);
        $this->criteriaBuilder->build();

        $expectedQuery = 'SELECT * FROM nothing WHERE field = test';
        $actualQuery   = $this->criteriaBuilder->getQueryBuilder()->getSQL();

        $this->assertEquals($expectedQuery, $actualQuery);
    }
}
