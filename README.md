This is a small part of a Symfony-based web application with the Symfony part stripped off. Only code checks and unit tests are available.

This is also an attempt at DDD and CQRS.

Full code is still a work in progress.

Build commands:

```
make test_unit
make code_check
```
