<?php

namespace Foobar\User\Query\Handler;

use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Query\QueryBuilder;
use Knp\Component\Pager\PaginatorInterface;
use Foobar\Common\Command\SimpleCommandHandler;
use Foobar\Common\Search\Criteria\DbalSearchCriteriaBuilder;
use Foobar\User\Entity\User;
use Foobar\User\Query\LoadUserSearch;
use Foobar\User\Query\LoadUserState;
use Foobar\User\Repository\UserRepository;
use Foobar\User\Search\Criteria;

final class UserQueryHandler extends SimpleCommandHandler
{
    private $repository;
    private $dbal;
    private $pager;

    public function __construct(
        UserRepository $repository,
        Connection $dbal,
        PaginatorInterface $pager
    ) {
        $this->repository = $repository;
        $this->dbal       = $dbal;
        $this->pager      = $pager;
    }

    public function handleLoadUserState(LoadUserState $query)
    {
        if (!$query->id) {
            $this->invalidCommand($query, 'Field required: id.');
        }

        $user = $this->repository->findById($query->id);

        return $user->getState();
    }

    public function handleLoadUserSearch(LoadUserSearch $query)
    {
        $criteriaBuilder = (new DbalSearchCriteriaBuilder())
            ->add(new Criteria\ByName($query->name))
            ->add(new Criteria\ByCity($query->city))
            ->add(new Criteria\ByCountry($query->country))
        ;

        $queryBuilder = $this->dbal->createQueryBuilder()
            ->select('*')
            ->from('users');

        $criteriaBuilder->setQueryBuilder($queryBuilder);
        $criteriaBuilder->build();

        return $this->pager->paginate(
            $criteriaBuilder->getQueryBuilder(),
            $query->page,
            $query->limit,
            ['dataTransformer' => $this->getResultTransformer()]
        );
    }

    private function getResultTransformer()
    {
        return function ($result) {
            return User::fromState($result)->getState();
        };
    }
}
