<?php

namespace Foobar\User\Query\Handler;

use Foobar\Common\Command\SimpleCommandHandler;
use Foobar\User\Query\LoadPhotoCollection;
use Foobar\User\Repository\PhotoRepository;

final class PhotoQueryHandler extends SimpleCommandHandler
{
    private $repository;

    public function __construct(PhotoRepository $repository)
    {
        $this->repository = $repository;
    }

    public function handleLoadPhotoCollection(LoadPhotoCollection $query)
    {
        if (!$query->userId) {
            $this->invalidCommand($query, 'Field required: userId');
        }

        $photos = $this->repository->findByUserId($query->userId);

        return array_map(function ($photo) {
            return $photo->getState();
        }, $photos);
    }
}
