<?php

namespace Foobar\User\Query;

use Foobar\Common\Command\Command;

class LoadPhotoCollection implements Command
{
    public $userId;

    public static function byUserId(string $userId)
    {
        $command = new static;

        $command->userId = $userId;

        return $command;
    }
}
