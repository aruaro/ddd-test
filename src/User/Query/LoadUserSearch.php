<?php

namespace Foobar\User\Query;

use Foobar\Common\Command\Command;
use Foobar\Common\Pagination\PagedCommandTrait;

class LoadUserSearch implements Command
{
    use PagedCommandTrait;

    public $name;
    public $city;
    public $country;
}
