<?php

namespace Foobar\User\Query;

use Foobar\Common\Command\Command;

class LoadUserState implements Command
{
    public $id;

    public static function byId(string $id)
    {
        $command = new static;

        $command->id = $id;

        return $command;
    }
}
