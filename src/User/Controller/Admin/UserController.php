<?php

namespace Foobar\User\Controller\Admin;

use Foobar\Common\Controller\BaseController;
use Foobar\User\Command\CreatePhoto;
use Foobar\User\Command\CreateUser;
use Foobar\User\Command\RemovePhoto;
use Foobar\User\Command\RemoveUser;
use Foobar\User\Command\UpdateUser;
use Foobar\User\Form\CreateUserForm;
use Foobar\User\Form\SearchUserForm;
use Foobar\User\Form\UpdateUserForm;
use Foobar\User\Query\LoadPhotoCollection;
use Foobar\User\Query\LoadUserSearch;
use Foobar\User\Query\LoadUserState;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class UserController extends BaseController
{
    public function index(Request $request): Response
    {
        $command = LoadUserSearch::byPage(
            $request->query->get('page', 1) ?: 1,
            $request->query->get('limit', 10) ?: 10
        );

        $form = $this->createForm(SearchUserForm::class, $command, ['method' => 'GET']);

        $form->handleRequest($request);

        $users = $this->commandBus->dispatch($command);

        return $this->render('admin/user/index.html.twig', [
            'users'       => $users,
            'form'        => $form->createView(),
        ]);
    }

    public function new(Request $request): Response
    {
        $command = new CreateUser();
        $form    = $this->createForm(CreateUserForm::class, $command);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $command->id = $id = $this->createId();

            $this->commandBus->dispatch($command);

            foreach ($command->photos as $photo) {
                $this->commandBus->dispatch(
                    new CreatePhoto($id, $photo['path'])
                );
            }

            return $this->redirectToRoute('admin_user_edit', ['id' => $command->id]);
        }

        return $this->render('admin/user/new.html.twig', [
            'user' => [],
            'form' => $form->createView(),
        ]);
    }

    public function edit(Request $request, string $id): Response
    {
        $user   = $this->commandBus->dispatch(LoadUserState::byId($id));
        $photos = $this->commandBus->dispatch(LoadPhotoCollection::byUserId($id));

        $command         = UpdateUser::fromState($user);
        $command->photos = $photos;

        $form = $this->createForm(UpdateUserForm::class, $command);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->commandBus->dispatch($command);

            foreach ($command->photos as $photo) {
                $this->commandBus->dispatch(
                    new CreatePhoto($id, $photo['path'])
                );
            }

            return $this->redirectToRoute('admin_user_edit', ['id' => $id]);
        }

        return $this->render('admin/user/edit.html.twig', [
            'user' => $user,
            'form' => $form->createView(),
        ]);
    }

    public function delete(Request $request, string $id): Response
    {
        if ($this->isCsrfTokenValid('delete'.$id, $request->request->get('_token'))) {
            $this->commandBus->dispatch(RemoveUser::byId($id));
        }

        return $this->redirectToRoute('admin_user_index');
    }
}
