<?php

namespace Foobar\User\Search\Criteria;

use Foobar\Common\Search\Criteria\SearchCriteria;

class ByCountry extends SearchCriteria
{
    public function __construct($value)
    {
        $this->field = 'country';
        $this->value = $value;
    }

    public function getExpression() : string
    {
        return 'users.country = :' . $this->field;
    }
}
