<?php

namespace Foobar\User\Search\Criteria;

use Foobar\Common\Search\Criteria\SearchCriteria;

class ByName extends SearchCriteria
{
    public function __construct($value)
    {
        $this->field = 'name';
        $this->value = implode('', ['%', $value, '%']);
    }

    public function getExpression() : string
    {
        return 'users.name LIKE :' . $this->field;
    }
}
