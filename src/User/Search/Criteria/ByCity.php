<?php

namespace Foobar\User\Search\Criteria;

use Foobar\Common\Search\Criteria\SearchCriteria;

class ByCity extends SearchCriteria
{
    public function __construct($value)
    {
        $this->field = 'city';
        $this->value = implode('', ['%', $value, '%']);
    }

    public function getExpression() : string
    {
        return 'users.city LIKE :' . $this->field;
    }
}
