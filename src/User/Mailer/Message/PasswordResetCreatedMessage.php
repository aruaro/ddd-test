<?php

namespace Foobar\User\Mailer\Message;

use Foobar\Common\Mailer\Message\MessageComposer;
use Foobar\Common\Mailer\Message\MessageComposerInterface;

final class PasswordResetCreatedMessage extends MessageComposer implements MessageComposerInterface
{
    protected $subject  = 'Your password reset link';
    protected $template = '';

    protected function getBody(array $data) : string
    {
        return sprintf(
            '<html><body>'.
            '<p>Hi there!</p>'.
            '<p>Here is the link to reset your password:</p>'.
            '<p>%s</p>'.
            '<p>If you have not requested to reset your password, please ignore this message.</p>'.
            '</body></html>',
            $data['reset_link']
        );
    }
}
