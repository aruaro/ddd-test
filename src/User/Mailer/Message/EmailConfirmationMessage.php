<?php

namespace Foobar\User\Mailer\Message;

use Foobar\Common\Mailer\Message\MessageComposer;
use Foobar\Common\Mailer\Message\MessageComposerInterface;

final class EmailConfirmationMessage extends MessageComposer implements MessageComposerInterface
{
    protected $subject  = 'Please confirm your email';
    protected $template = '';

    protected function getBody(array $data) : string
    {
        return sprintf(
            '<html><body>'.
            '<p>Hi there!</p>'.
            '<p>Please confirm your email by going to this link:</p>'.
            '<p>%s</p>'.
            '</body></html>',
            $data['activationUrl']
        );
    }
}
