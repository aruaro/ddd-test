<?php

namespace Foobar\User\Mailer\Message;

use Foobar\Common\Mailer\Message\MessageComposer;
use Foobar\Common\Mailer\Message\MessageComposerInterface;

final class PasswordResetCheckedMessage extends MessageComposer implements MessageComposerInterface
{
    protected $subject  = 'Your password has been updated';
    protected $template = '';

    protected function getBody(array $data) : string
    {
        return
            '<html><body>'.
            '<p>Hi there!</p>'.
            '<p>Your password has been updated.</p>'.
            '<p>You may contact us if you did not reset your password.</p>'.
            '</body></html>'
        ;
    }
}
