<?php

namespace Foobar\User\ValueObject;

class SecureHash
{
    private $hash;

    public function __construct(string $hash = '')
    {
        $this->hash = $hash;
    }

    public static function generate(int $byteLength = 16)
    {
        $hash = new static();

        $hash->hash = bin2hex(random_bytes($byteLength));

        return $hash;
    }

    public function getHash() : string
    {
        return $this->hash;
    }
}
