<?php

namespace Foobar\User\ValueObject;

class Status
{
    const INACTIVE  = 'inactive';
    const ACTIVE    = 'active';
    const SUSPENDED = 'suspended';
    const BANNED    = 'banned';

    private $status;

    public function __construct($status)
    {
        $this->status = $status;
    }

    public static function initialize() : self
    {
        return new static(self::INACTIVE);
    }

    public static function fromJson(string $json) : self
    {
        return new static(json_decode($json, true));
    }

    public function all()
    {
        return [
            self::ACTIVE,
            self::INACTIVE,
            self::SUSPENDED,
            self::BANNED
        ];
    }

    public function getStatus()
    {
        return $this->status;
    }

    public function activate()
    {
        $this->status = self::ACTIVE;
    }

    public function deactivate()
    {
        $this->status = self::INACTIVE;
    }

    public function suspend()
    {
        $this->status = self::SUSPENDED;
    }

    public function ban()
    {
        $this->status = self::BANNED;
    }
}
