<?php

namespace Foobar\User\ValueObject;

class Flags
{
    const DEFAULT  = 0b00000000;
    const VERIFIED = 0b00000001;

    private $flags;

    public function __construct($flags = self::DEFAULT)
    {
        $this->flags = $flags;
    }

    public static function initialize() : self
    {
        return new static(self::DEFAULT);
    }

    public static function fromJson(string $json) : self
    {
        return new static(json_decode($json, true));
    }

    public function getFlags()
    {
        return $this->flags;
    }

    public function verify()
    {
        $this->flags = $this->flags ^ self::VERIFIED;
    }
}
