<?php

namespace Foobar\User\Event;

class UserActivated extends UserEvent
{
    const NAME = UserEvent::ACTIVATED;

    private $userId;

    public function __construct($userId)
    {
        $this->userId = $userId;
    }

    public function userId() : string
    {
        return $this->userId;
    }
}
