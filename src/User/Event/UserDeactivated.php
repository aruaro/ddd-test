<?php

namespace Foobar\User\Event;

class UserDeactivated extends UserEvent
{
    const NAME = UserEvent::DEACTIVATED;

    private $userId;

    public function __construct($userId)
    {
        $this->userId = $userId;
    }

    public function userId() : string
    {
        return $this->userId;
    }
}
