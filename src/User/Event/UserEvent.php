<?php

namespace Foobar\User\Event;

use Symfony\Component\EventDispatcher\Event as BaseEvent;

class UserEvent extends BaseEvent
{
    const LOADED      = 'user.loaded';
    const CREATED     = 'user.created';
    const UPDATED     = 'user.updated';
    const ACTIVATED   = 'user.activated';
    const DEACTIVATED = 'user.deactivated';

    const PASSWORD_RESET_CREATED = 'security.password_reset.created';
    const PASSWORD_RESET_CHECKED = 'security.password_reset.checked';
}
