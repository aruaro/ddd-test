<?php

namespace Foobar\User\Event;

use Foobar\User\Entity\PasswordReset;

class PasswordResetCreated extends UserEvent
{
    const NAME = UserEvent::PASSWORD_RESET_CREATED;

    private $passwordReset;

    public function __construct(PasswordReset $passwordReset)
    {
        $this->passwordReset = $passwordReset;
    }

    public function getPasswordReset() : PasswordReset
    {
        return $this->passwordReset;
    }
}
