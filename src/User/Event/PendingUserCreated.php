<?php

namespace Foobar\User\Event;

use Foobar\User\Entity\PendingUser;

class PendingUserCreated extends UserEvent
{
    const NAME = UserEvent::CREATED;

    private $pendingUser;

    public function __construct(PendingUser $pendingUser)
    {
        $this->pendingUser = $pendingUser;
    }

    public function getUser() : PendingUser
    {
        return $this->pendingUser;
    }
}
