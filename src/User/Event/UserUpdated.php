<?php

namespace Foobar\User\Event;

use Foobar\User\Entity\User;

class UserUpdated extends UserEvent
{
    const NAME = UserEvent::UPDATED;

    private $oldUser;
    private $newUser;

    public function __construct(User $oldUser, User $newUser)
    {
        $this->oldUser = $oldUser;
        $this->newUser = $newUser;
    }

    public function getOldUser() : User
    {
        return $this->oldUser;
    }

    public function getNewUser() : User
    {
        return $this->newUser;
    }
}
