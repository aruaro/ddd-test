<?php

namespace Foobar\User\Event;

use Foobar\User\Entity\PasswordReset;

class PasswordResetChecked extends UserEvent
{
    const NAME = UserEvent::PASSWORD_RESET_CHECKED;

    private $passwordReset;

    public function __construct(PasswordReset $passwordReset)
    {
        $this->passwordReset = $passwordReset;
    }

    public function getPasswordReset() : PasswordReset
    {
        return $this->passwordReset;
    }
}
