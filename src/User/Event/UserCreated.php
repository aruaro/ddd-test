<?php

namespace Foobar\User\Event;

use Foobar\User\Entity\User;

class UserCreated extends UserEvent
{
    const NAME = UserEvent::CREATED;

    private $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    public function getUser() : User
    {
        return $this->user;
    }
}
