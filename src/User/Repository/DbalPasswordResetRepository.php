<?php

namespace Foobar\User\Repository;

use Doctrine\DBAL\Connection;
use Foobar\User\Entity\PasswordReset;
use Foobar\User\Exception\PasswordResetNotFound;

class DbalPasswordResetRepository implements PasswordResetRepository
{
    protected $dbal;

    public function __construct(Connection $dbal)
    {
        $this->dbal = $dbal;
    }

    public function findByEmail(string $email)
    {
        $qb = $this->dbal->createQueryBuilder();

        $statement = $qb->select('pr.*, u.id AS user_id')
            ->from('user_password_resets', 'pr')
            ->rightJoin('pr', 'users', 'u', 'pr.user_id = u.id')
            ->where('email = :email')
            ->setParameter(':email', $email)
            ->execute();

        if ($result = $statement->fetch()) {
            return PasswordReset::create($result['user_id']);
        }

        throw PasswordResetNotFound::fromEmptyUser();
    }

    public function findByResetHash(string $resetHash)
    {
        $dateFormat = $this->dbal->getDatabasePlatform()->getDateTimeFormatString();
        $expiresAt  = (new \DateTimeImmutable())->format($dateFormat);

        $qb = $this->dbal->createQueryBuilder();

        $statement = $qb->select('*')
            ->from('user_password_resets')
            ->where('reset_hash = :resetHash')
            ->andWhere('expires_at >= :expiresAt')
            ->setParameter(':expiresAt', $expiresAt)
            ->setParameter(':resetHash', $resetHash)
            ->execute();

        if ($result = $statement->fetch()) {
            return PasswordReset::fromState($result);
        }

        throw PasswordResetNotFound::fromResetHash($resetHash);
    }

    public function create(PasswordReset $passwordReset)
    {
        $state = $this->normalizePasswordReset($passwordReset);

        $this->dbal->insert('user_password_resets', $state);
    }

    public function update(PasswordReset $passwordReset)
    {
        $state = $this->normalizePasswordReset($passwordReset);

        $this->dbal->update('user_password_resets', $state, [
            'reset_hash' => $passwordReset->getResetHash()
        ]);
    }

    public function delete(PasswordReset $passwordReset)
    {
        $this->dbal->delete('user_password_resets', [
            'reset_hash' => $passwordReset->getResetHash()
        ]);
    }

    private function normalizePasswordReset(PasswordReset $passwordReset)
    {
        $dateFormat = $this->dbal->getDatabasePlatform()->getDateTimeFormatString();
        $state      = $passwordReset->getState();

        $state['expires_at'] = $passwordReset->getExpiresAt()->format($dateFormat);
        $state['checked']    = $passwordReset->isChecked() ? '1' : 0;

        return $state;
    }
}
