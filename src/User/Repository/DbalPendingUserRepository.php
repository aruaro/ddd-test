<?php

namespace Foobar\User\Repository;

use Doctrine\DBAL\Connection;
use Foobar\User\Entity\PendingUser;
use Foobar\User\Exception\PendingUserNotFound;

class DbalPendingUserRepository implements PendingUserRepository
{
    protected $dbal;

    public function __construct(Connection $dbal)
    {
        $this->dbal = $dbal;
    }

    public function findByToken(string $token)
    {
        return $this->finyByColumn('token', $token);
    }

    public function findByEmail(string $email)
    {
        return $this->finyByColumn('email', $email);
    }

    private function finyByColumn(string $column, $value)
    {
        $statement = $this->dbal->createQueryBuilder()
            ->select('*')
            ->from('users_pending')
            ->where($column . ' = :value')
            ->andWhere('checked = 1')
            ->setParameter(':value', $value)
            ->execute();

        if ($result = $statement->fetch()) {
            return PendingUser::fromState($result);
        }

        throw PendingUserNotFound::withColumn($column, $value);
    }

    public function create(PendingUser $pendingUser)
    {
        $state = $this->normalizePendingUser($pendingUser);

        $this->dbal->insert('users_pending', $state);
    }

    public function update(PendingUser $pendingUser)
    {
        $state = $this->normalizePendingUser($pendingUser);

        $this->dbal->update('users_pending', $state, [
            'token' => $pendingUser->getResetHash()
        ]);
    }

    public function delete(PendingUser $pendingUser)
    {
        $this->dbal->delete('users_pending', [
            'token' => $pendingUser->getResetHash()
        ]);
    }

    private function normalizePendingUser(PendingUser $pendingUser)
    {
        $dateFormat = $this->dbal->getDatabasePlatform()->getDateTimeFormatString();
        $state      = $pendingUser->getState();

        $state['expires_at'] = $pendingUser->getExpiresAt()->format($dateFormat);
        $state['checked']    = $pendingUser->isChecked() ? '1' : 0;

        return $state;
    }
}
