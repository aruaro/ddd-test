<?php

namespace Foobar\User\Repository;

use Foobar\Common\Search\Criteria\SearchCriteriaBuilder;
use Foobar\User\Entity\User;

interface UserRepository
{
    public function findAll();

    public function findById(string $id);

    public function findByEmail(string $email);

    public function findByLogin(string $login);

    public function create(User $user);

    public function update(User $user);

    public function delete(User $user);
}
