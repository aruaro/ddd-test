<?php

namespace Foobar\User\Repository;

use Foobar\User\Entity\PasswordReset;

interface PasswordResetRepository
{
    public function findByEmail(string $email);

    public function findByResetHash(string $resetHash);

    public function create(PasswordReset $passwordReset);

    public function update(PasswordReset $passwordReset);

    public function delete(PasswordReset $passwordReset);
}
