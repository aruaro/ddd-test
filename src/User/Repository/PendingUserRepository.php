<?php

namespace Foobar\User\Repository;

use Foobar\User\Entity\PendingUser;

interface PendingUserRepository
{
    public function findByToken(string $token);

    public function findByEmail(string $email);

    public function create(PendingUser $pendingUser);

    public function update(PendingUser $pendingUser);

    public function delete(PendingUser $pendingUser);
}
