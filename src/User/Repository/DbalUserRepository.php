<?php

namespace Foobar\User\Repository;

use Doctrine\DBAL\Connection;
use Foobar\User\Entity\Profile;
use Foobar\User\Entity\User;
use Foobar\User\Exception\UserNotFound;

class DbalUserRepository implements UserRepository
{
    protected $dbal;

    public function __construct(Connection $dbal)
    {
        $this->dbal = $dbal;
    }

    public function findAll()
    {
        $statement = $this->dbal->createQueryBuilder()
            ->from('users', 'u')
            ->execute();

        return array_map(function ($result) {
            return User::fromState($result);
        }, $statement->fetchAll());
    }

    public function findById(string $id)
    {
        return $this->findByColumn('id', $id);
    }

    public function findByEmail(string $email)
    {
        return $this->findByColumn('email', $email);
    }

    public function findByLogin(string $login, ?string $id = null)
    {
        $column = !empty($id) ? 'id' : 'email';
        $value  = !empty($id) ? $id : $login;

        $statement = $this->dbal->createQueryBuilder()
            ->select('u.id, u.email as login, uc.*')
            ->from('users', 'u')
            ->leftJoin('u', 'user_credentials', 'uc', 'u.id = uc.user_id')
            ->where($column . ' = :param')
            ->setParameter(':param', $value)
            ->execute();

        if ($result = $statement->fetch()) {
            return $result;
        }

        throw UserNotFound::withColumn($column, $value);
    }

    public function create(User $user)
    {
        $this->dbal->transactional(function () use ($user) {
            $state = $this->normalizeUser($user);

            $this->dbal->insert('users', $state['user']);
            $this->dbal->insert('user_credentials', $state['credentials']);
        });
    }

    public function update(User $user)
    {
        $this->dbal->transactional(function () use ($user) {
            $state = $this->normalizeUser($user);

            unset($state['user']['id']);
            unset($state['credentials']['user_id']);

            $this->dbal->update('users', $state['user'], ['id' => $user->getId()]);

            $this->dbal->update(
                'user_credentials',
                $state['credentials'],
                ['user_id' => $user->getId()]
            );
        });
    }

    public function delete(User $user)
    {
        $this->dbal->transactional(function () use ($user) {
            $this->dbal->delete('users', ['id' => $user->getId()]);
            $this->dbal->delete('user_credentials', ['user_id' => $user->getId()]);
        });
    }

    private function findByColumn(string $column, string $value)
    {
        $statement =  $this->dbal->createQueryBuilder()
            ->select('*')
            ->from('users')
            ->where($column . ' = :param')
            ->setParameter(':param', $value)
            ->execute();

        if ($result = $statement->fetch()) {
            return User::fromState($result);
        }

        throw UserNotFound::withColumn($column, $value);
    }

    private function normalizeUser(User $user)
    {
        $profile    = $user->getProfile();
        $dateFormat = $this->dbal->getDatabasePlatform()->getDateTimeFormatString();

        return [
            'user' => [
                'id'         => $user->getId(),
                'email'      => $user->getEmail(),
                'name'       => $profile->getName(),
                'street'     => $profile->getAddress()->getStreet(),
                'city'       => $profile->getAddress()->getCity(),
                'locality'   => $profile->getAddress()->getLocality(),
                'country'    => $profile->getAddress()->getCountry(),
                'zipcode'    => $profile->getAddress()->getZipcode(),
                'status'     => $user->getStatus(),
                'flags'      => $user->getFlags(),
                'created_at' => $user->getCreatedAt()->format($dateFormat),
                'updated_at' => $user->getUpdatedAt()->format($dateFormat),
            ],
            'credentials' => [
                'user_id'       => $user->getId(),
                'password'      => $user->getPassword(),
                'last_login_at' => (new \DateTimeImmutable())->format($dateFormat),
            ]
        ];
    }
}
