<?php

namespace Foobar\User\Repository;

use Foobar\User\Entity\Photo;

interface PhotoRepository
{
    public function findById(string $userId, string $path);

    public function findByUserId(string $userId);

    public function create(Photo $photo);

    public function delete(Photo $photo);
}
