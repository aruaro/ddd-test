<?php

namespace Foobar\User\Repository;

use Doctrine\DBAL\Connection;
use Foobar\User\Entity\Photo;

class DbalPhotoRepository implements PhotoRepository
{
    protected $dbal;

    public function __construct(Connection $dbal)
    {
        $this->dbal = $dbal;
    }

    public function findById(string $userId, string $path)
    {
        $statement = $this->dbal->createQueryBuilder()
            ->select('*')
            ->from('user_photos')
            ->where('user_id = :userId')
            ->setParameter(':userId', $userId)
            ->execute();

        if ($result = $statement->fetch()) {
            return Photo::fromState($result);
        }
    }

    public function findByUserId(string $userId)
    {
        $statement = $this->dbal->createQueryBuilder()
            ->select('*')
            ->from('user_photos')
            ->where('user_id = :userId')
            ->setParameter(':userId', $userId)
            ->execute();

        return array_map(function ($result) {
            return Photo::fromState($result);
        }, $statement->fetchAll());
    }

    public function create(Photo $photo)
    {
        if ($this->dbal->getDriver()->getName() == 'pdo_sqlite') {
            $sql = 'INSERT OR IGNORE INTO '
                . 'user_photos (user_id, path, mimetype, is_main) '
                . 'VALUES (?, ?, ?, ?)';
        } else {
            $sql = 'INSERT IGNORE INTO '
                . 'user_photos (user_id, path, mimetype, is_main) '
                . 'VALUES (?, ?, ?, ?)';
        }

        $this->dbal->prepare($sql)
            ->execute(array_values($photo->getState()));
    }

    public function delete(Photo $photo)
    {
        $this->dbal->delete('user_photos', [
            'user_id' => $photo->getUserId(),
            'path'    => $photo->getPath()
        ]);
    }
}
