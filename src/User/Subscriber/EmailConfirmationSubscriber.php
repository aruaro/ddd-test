<?php

namespace Foobar\User\Subscriber;

use Psr\Log\LoggerInterface;
use Foobar\Common\Mailer\Mailer;
use Foobar\User\Entity\User;
use Foobar\User\Event\UserCreated;
use Foobar\User\Event\UserUpdated;
use Foobar\User\Mailer\Message\EmailConfirmationMessage;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\PostResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Webmozart\Assert\Assert;

class EmailConfirmationSubscriber implements EventSubscriberInterface
{
    /** @var EventDispatcherInterface */
    private $eventDispatcher;
    /** @var UrlGeneratorInterface */
    private $urlGenerator;
    /** @var Mailer */
    private $mailer;
    /** @var LoggerInterface|null */
    private $logger;

    private $user;
    private $adminEmail;

    public function __construct(
        EventDispatcherInterface $eventDispatcher,
        UrlGeneratorInterface $urlGenerator,
        Mailer $mailer,
        string $adminEmail,
        ?LoggerInterface $logger = null
    ) {
        $this->eventDispatcher = $eventDispatcher;
        $this->urlGenerator    = $urlGenerator;
        $this->mailer          = $mailer;
        $this->adminEmail      = $adminEmail;
        $this->logger          = $logger;
    }

    public static function getSubscribedEvents() : array
    {
        if ($_ENV['APP_ENV'] == 'test') {
            return [];
        }

        return [
            UserCreated::NAME => 'onUserCreated',
            UserUpdated::NAME => 'onUserUpdated',
        ];
    }

    public function onUserCreated(UserCreated $event)
    {
        $this->user = $event->getUser();

        $this->eventDispatcher->addListener(
            KernelEvents::TERMINATE,
            [$this, 'onKernelTerminate']
        );
    }

    public function onUserUpdated(UserUpdated $event)
    {
        $this->user = $event->getNewUser();

        $this->eventDispatcher->addListener(
            KernelEvents::TERMINATE,
            [$this, 'onKernelTerminate']
        );
    }

    public function onKernelTerminate(PostResponseEvent $event)
    {
        Assert::isInstanceOf($this->user, User::class);

        $activationUrl = $this->urlGenerator->generate(
            'app_user_activate',
            ['token' => 'token'],
            UrlGeneratorInterface::ABSOLUTE_URL
        );

        $message = (new EmailConfirmationMessage)->compose([
            'sender'   => [
                'name'  => 'Foobar',
                'email' => $this->adminEmail
            ],
            'receiver' => [
                'name'  => $this->user->getProfile()->getName(),
                'email' => $this->user->getEmail()
            ],
            'data' => ['activationUrl' => $activationUrl],
        ]);

        $this->mailer->send($message);

        if ($this->logger) {
            $this->logger->debug('Email confirmation sent.', ['user' => $this->user->getId()]);
        }
    }
}
