<?php

namespace Foobar\User\Subscriber;

use Psr\Log\LoggerInterface;
use Foobar\Common\Mailer\Mailer;
use Foobar\User\Entity\PasswordReset;
use Foobar\User\Event\PasswordResetChecked;
use Foobar\User\Event\PasswordResetCreated;
use Foobar\User\Mailer\Message\PasswordResetCheckedMessage;
use Foobar\User\Mailer\Message\PasswordResetCreatedMessage;
use Foobar\User\Repository\UserRepository;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\PostResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Webmozart\Assert\Assert;

class PasswordResetMailerSubscriber implements EventSubscriberInterface
{
    /** @var EventDispatcherInterface */
    private $eventDispatcher;
    /** @var UrlGeneratorInterface */
    private $urlGenerator;
    /** @var UserRepository */
    private $userRepository;
    /** @var Mailer */
    private $mailer;
    /** @var LoggerInterface|null */
    private $logger;

    private $passwordReset;
    private $adminEmail;
    private $message;

    public function __construct(
        EventDispatcherInterface $eventDispatcher,
        UrlGeneratorInterface $urlGenerator,
        UserRepository $userRepository,
        Mailer $mailer,
        string $adminEmail,
        ?LoggerInterface $logger = null
    ) {
        $this->eventDispatcher = $eventDispatcher;
        $this->urlGenerator    = $urlGenerator;
        $this->userRepository  = $userRepository;
        $this->mailer          = $mailer;
        $this->adminEmail      = $adminEmail;
        $this->logger          = $logger;
    }

    public static function getSubscribedEvents() : array
    {
        if ($_ENV['APP_ENV'] == 'test') {
            return [];
        }

        return [
            PasswordResetCreated::NAME => 'onPasswordResetCreated',
            PasswordResetChecked::NAME => 'onPasswordResetChecked',
        ];
    }

    public function onPasswordResetCreated(PasswordResetCreated $event)
    {
        $this->passwordReset = $event->getPasswordReset();
        $this->message       = new PasswordResetCreatedMessage();

        $this->eventDispatcher->addListener(
            KernelEvents::TERMINATE,
            [$this, 'onKernelTerminate']
        );
    }

    public function onPasswordResetChecked(PasswordResetChecked $event)
    {
        $this->passwordReset = $event->getPasswordReset();
        $this->message       = new PasswordResetCheckedMessage();

        $this->eventDispatcher->addListener(
            KernelEvents::TERMINATE,
            [$this, 'onKernelTerminate']
        );
    }

    public function onKernelTerminate(PostResponseEvent $event)
    {
        Assert::isInstanceOf($this->passwordReset, PasswordReset::class);

        $user = $this->userRepository->findById($this->passwordReset->getUserId());

        $messageParams = [
            'sender'   => [
                'name'  => 'Foobar',
                'email' => $this->adminEmail
            ],
            'receiver' => [
                'name'  => $user->getProfile()->getName(),
                'email' => $user->getEmail()
            ]
        ];

        if ($this->message instanceof PasswordResetCreatedMessage) {
            $resetLink = $this->urlGenerator->generate(
                'app_password_reset_check',
                ['resetHash' => $this->passwordReset->getResetHash()]
            );

            $messageParams['data'] = ['reset_link' => $event->getRequest()->getHost() . $resetLink];
        }

        $this->mailer->send(
            $this->message->compose($messageParams)
        );

        if ($this->logger) {
            $this->logger->debug('Password reset email sent.', ['user' => $user->getId()]);
        }
    }
}
