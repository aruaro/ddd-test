<?php

namespace Foobar\User\Subscriber;

use Psr\Log\LoggerInterface;
use Foobar\Common\Security\SecurityUser as SecurityUser;
use Foobar\User\Entity\User;
use Foobar\User\Event\PasswordResetChecked;
use Foobar\User\Event\UserCreated;
use Foobar\User\Repository\UserRepository;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Core\Role\Role;

class UserAutoLoginSubscriber implements EventSubscriberInterface
{
    /** @var UserRepository */
    private $userRepository;
    /** @var TokenStorageInterface */
    private $tokenStorage;
    /** @var LoggerInterface|null */
    private $logger;

    private $firewallContext;

    public function __construct(
        UserRepository $userRepository,
        TokenStorageInterface $tokenStorage,
        ?LoggerInterface $logger = null
    ) {
        $this->userRepository  = $userRepository;
        $this->tokenStorage    = $tokenStorage;
        $this->logger          = $logger;
    }

    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::REQUEST      => 'onKernelRequest',
            UserCreated::NAME          => 'onUserCreated',
            PasswordResetChecked::NAME => 'onPasswordResetChecked',
        ];
    }

    public function onKernelRequest(GetResponseEvent $event)
    {
        $this->firewallContext = $event->getRequest()->attributes->get('_firewall_context');
    }

    public function onUserCreated(UserCreated $event)
    {
        $this->autoLogin($event->getUser());
    }

    public function onPasswordResetChecked(PasswordResetChecked $event)
    {
        $userId = $event->getPasswordReset()->getUserId();

        $this->autoLogin(
            $this->userRepository->findById($userId)
        );
    }

    private function autoLogin(User $user)
    {
        $role         = new Role('ROLE_USER');
        $securityUser = new SecurityUser(
            $user->getId(),
            $user->getEmail(),
            $user->getPassword(),
            [$role->getRole()]
        );

        $this->tokenStorage->setToken(
            new UsernamePasswordToken($securityUser, null, $this->firewallContext, [$role])
        );

        if ($this->logger) {
            $this->logger->debug('User authenticated automatically.', ['user' => $user->getId()]);
        }
    }
}
