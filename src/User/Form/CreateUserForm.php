<?php

namespace Foobar\User\Form;

use Foobar\Common\Addressing\Form\AddressType;
use Foobar\User\Command\CreateUser;
use Foobar\User\Exception\UserNotFound;
use Foobar\User\Repository\UserRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\CountryType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormError;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

class CreateUserForm extends AbstractType
{
    private $repository;

    public function __construct(UserRepository $repository)
    {
        $this->repository = $repository;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email', EmailType::class)
            ->add('password', RepeatedType::class, [
                'type'            => PasswordType::class,
                'required'        => false,
                'first_options'   => ['label' => 'Password'],
                'second_options'  => ['label' => 'Repeat Password'],
            ])

            ->add('name', TextType::class, ['required' => false])
            ->add('address', AddressType::class, ['required' => false])

            ->add('photos', CollectionType::class, [
                'error_bubbling' => true,
                'allow_add'      => true,
                // 'allow_delete'   => true,
                'prototype'      => true,
                'entry_type'     => Type\PhotoType::class,
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'label'       => false,
            'data_class'  => CreateUser::class,
            'constraints' => [
                new Assert\Callback([$this, 'validate'])
            ]
        ]);
    }

    public function getBlockPrefix()
    {
        return '';
    }

    public function validate(CreateUser $command, ExecutionContextInterface $context)
    {
        $form = $context->getObject();

        if ($this->isEmailTaken($command->email)) {
            $form->get('email')->addError(new FormError('Email is already taken.'));
        }
    }

    private function isEmailTaken(string $email)
    {
        try {
            return $this->repository->findByEmail($email) ? true : false;
        } catch (UserNotFound $exception) {
            return false;
        }
    }
}
