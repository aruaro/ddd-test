<?php

namespace Foobar\User\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\DataMapperInterface;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PhotoCollectionType extends AbstractType implements DataMapperInterface
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $photos = [];
        $main   = null;

        foreach ($options['data'] ?? [] as $index => $photo) {
            if ($photo['is_main']) {
                $main = $index;
            }

            $photos[] = $photo['path'];
        }

        $builder
            ->add('main', ChoiceType::class, [
                'data'         => $main,
                'choices'      => array_keys($photos),
                'choice_label' => false,
                'expanded'     => true,
                'label'        => false,
            ])
            ->add('paths', CollectionType::class, [
                'data'           => $photos,
                'error_bubbling' => true,
                'allow_add'      => true,
                'allow_delete'   => true,
                'prototype'      => true,
                'entry_type'     => TextType::class,
                'entry_options'  => ['label' => false]
            ])

            ->setDataMapper($this)

            ->addEventListener(FormEvents::PRE_SUBMIT, function (FormEvent $event) {
                $form = $event->getForm();

                if ($form->has('main')) {
                    $form->remove('main');
                    $form->add('main', ChoiceType::class, [
                        'choices'      => array_keys($event->getData()['paths'] ?? []),
                        'choice_label' => false,
                        'expanded'     => true,
                        'label'        => false,
                    ]);
                }
            })
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'label'          => false,
            'error_bubbling' => true,
        ]);
    }

    public function getBlockPrefix()
    {
        return '';
    }

    public function mapDataToForms($photos, $forms)
    {
        return;
    }

    public function mapFormsToData($forms, &$photos)
    {
        $forms = iterator_to_array($forms);
        $paths = $forms['paths']->getData();

        foreach ($paths as $index => $path) {
            $photos[$index]['path'] = $path;
        }

        foreach ($photos as $index => $photo) {
            $photos[$index]['is_main'] = $index == $forms['main']->getData() ? true : false;
        }

        foreach ($photos as $index => $photo) {
            if (!array_key_exists($index, $paths)) {
                $photos[$index]['is_main'] = 'deleted';
            }
        }
    }
}
