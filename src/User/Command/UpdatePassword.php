<?php

namespace Foobar\User\Command;

use Foobar\Common\Command\Command;

class UpdatePassword implements Command
{
    public $id;
    public $password;
}
