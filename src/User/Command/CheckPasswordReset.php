<?php

namespace Foobar\User\Command;

use Foobar\Common\Command\Command;

class CheckPasswordReset implements Command
{
    public $resetHash;

    public static function byResetHash(string $resetHash)
    {
        $command = new static;

        $command->resetHash = $resetHash;

        return $command;
    }
}
