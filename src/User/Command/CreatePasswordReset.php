<?php

namespace Foobar\User\Command;

use Foobar\Common\Command\Command;

class CreatePasswordReset implements Command
{
    public $email;
}
