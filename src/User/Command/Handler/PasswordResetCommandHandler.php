<?php

namespace Foobar\User\Command\Handler;

use Foobar\Common\Command\EventDispatchingCommandHandler;
use Foobar\User\Command\CheckPasswordReset;
use Foobar\User\Command\CreatePasswordReset;
use Foobar\User\Entity\PasswordReset;
use Foobar\User\Event\PasswordResetChecked;
use Foobar\User\Event\PasswordResetCreated;
use Foobar\User\Exception\PasswordResetNotFound;
use Foobar\User\Repository\PasswordResetRepository;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class PasswordResetCommandHandler extends EventDispatchingCommandHandler
{
    /** @var PasswordResetRepository */
    private $repository;
    /** @var SessionInterface */
    private $session;

    public function __construct(PasswordResetRepository $repository, SessionInterface $session)
    {
        $this->repository = $repository;
        $this->session    = $session;
    }

    public function handleCreatePasswordReset(CreatePasswordReset $command)
    {
        $passwordReset = $this->repository->findByEmail($command->email);

        $this->repository->create($passwordReset);

        $this->dispatchEvent(new PasswordResetCreated($passwordReset));
    }

    public function handleCheckPasswordReset(CheckPasswordReset $command)
    {
        $passwordReset = $this->repository->findByResetHash($command->resetHash);

        $passwordReset->markChecked();

        $this->session->set('reset_user_id', $passwordReset->getUserId());

        $this->repository->update($passwordReset);

        $this->dispatchEvent(new PasswordResetChecked($passwordReset));
    }
}
