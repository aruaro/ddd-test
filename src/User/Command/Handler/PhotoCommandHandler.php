<?php

namespace Foobar\User\Command\Handler;

use Foobar\Common\Command\SimpleCommandHandler;
use Foobar\User\Command\CreatePhoto;
use Foobar\User\Command\RemovePhoto;
use Foobar\User\Entity\Photo;
use Foobar\User\Repository\PhotoRepository;

final class PhotoCommandHandler extends SimpleCommandHandler
{
    private $repository;

    public function __construct(PhotoRepository $repository)
    {
        $this->repository = $repository;
    }

    public function handleCreatePhoto(CreatePhoto $command)
    {
        $photo = $this->repository->findById($command->userId, $command->path);

        if ($photo) {
            $this->repository->delete($photo);
        }

        $photo = Photo::create($command->userId, $command->path, $command->mimetype);

        if ($command->isMain) {
            $photo->setToMain();
        }

        $this->repository->create($photo);
    }

    public function handleRemovePhoto(RemovePhoto $command)
    {
        $photo = $this->repository->findById($command->userId, $command->path);

        $this->repository->delete($photo);
    }
}
