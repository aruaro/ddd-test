<?php

namespace Foobar\User\Command\Handler;

use Foobar\Common\Command\EventDispatchingCommandHandler;
use Foobar\User\Command\CreatePendingUser;
use Foobar\User\Command\RemovePendingUser;
use Foobar\User\Entity\PendingUser;
use Foobar\User\Event\PendingUserCreated;
use Foobar\User\Repository\PendingUserRepository;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class PendingUserCommandHandler extends EventDispatchingCommandHandler
{
    private $repository;
    private $passwordEncoder;

    public function __construct(
        PendingUserRepository $repository,
        UserPasswordEncoderInterface $passwordEncoder
    ) {
        $this->repository      = $repository;
        $this->passwordEncoder = $passwordEncoder;
    }

    public function handleCreatePendingUser(CreatePendingUser $command)
    {
        $pendingUser = PendingUser::create($command->email, $command->password);

        $this->repository->create($pendingUser);

        $this->dispatchEvent(new PendingUserCreated($pendingUser));
    }

    public function handleRemovePendingUser(RemovePendingUser $command)
    {
        $pendingUser = $this->repository->findByEmail($command->email);

        $this->repository->delete($pendingUser);
    }
}
