<?php

namespace Foobar\User\Command\Handler;

use Foobar\Common\Addressing\ValueObject\Address;
use Foobar\Common\Command\EventDispatchingCommandHandler;
use Foobar\Common\Security\SecurityUser as SecurityUser;
use Foobar\User\Command\ActivateUser;
use Foobar\User\Command\CreateUser;
use Foobar\User\Command\DeactivateUser;
use Foobar\User\Command\RemoveUser;
use Foobar\User\Command\UpdatePassword;
use Foobar\User\Command\UpdateUser;
use Foobar\User\Entity\Profile;
use Foobar\User\Entity\User;
use Foobar\User\Event\UserCreated;
use Foobar\User\Event\UserUpdated;
use Foobar\User\Repository\UserRepository;
use Foobar\User\Search\Criteria;
use Foobar\User\ValueObject\Flags;
use Foobar\User\ValueObject\Status;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

final class UserCommandHandler extends EventDispatchingCommandHandler
{
    private $repository;
    private $passwordEncoder;

    public function __construct(
        UserRepository $repository,
        UserPasswordEncoderInterface $passwordEncoder
    ) {
        $this->repository      = $repository;
        $this->passwordEncoder = $passwordEncoder;
    }

    public function handleCreateUser(CreateUser $command)
    {
        $encodedPassword = $this->passwordEncoder->encodePassword(
            new SecurityUser($command->id, $command->email, $command->password),
            $command->password
        );

        $user = User::create($command->id, $command->email, $encodedPassword);

        $user->getProfile()->update(
            $command->name ?? 'Foobar User',
            Address::fromState($command->address)
        );

        $this->repository->create($user);

        $this->dispatchEvent(new UserCreated($user));
    }

    public function handleUpdateUser(UpdateUser $command)
    {
        $oldUser = $this->repository->findById($command->id);
        $newUser = clone $oldUser;

        $newUser->changeEmail($command->email);

        $newUser->getProfile()->update(
            $command->name,
            Address::fromState($command->address)
        );

        $this->repository->update($newUser);

        $this->dispatchEvent(new UserUpdated($oldUser, $newUser));
    }

    public function handleUpdatePassword(UpdatePassword $command)
    {
        $user = $this->repository->findById($command->id);

        $encodedPassword = $this->passwordEncoder->encodePassword(
            new SecurityUser($command->id, $user->getEmail(), $command->password),
            $command->password
        );

        $user->changePassword($encodedPassword);

        $this->repository->update($user);
    }

    public function handleRemoveUser(RemoveUser $command)
    {
        $user = $this->repository->findById($command->id);

        $this->repository->delete($user);
    }

    public function handleActivateUser(ActivateUser $command)
    {
        $user = $this->repository->findById($command->id);
    }

    public function handleDeactivateUser(DeactivateUser $command)
    {
        $user = $this->repository->findById($command->id);
    }
}
