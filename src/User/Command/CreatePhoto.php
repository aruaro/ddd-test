<?php

namespace Foobar\User\Command;

use Foobar\Common\Command\Command;

class CreatePhoto implements Command
{
    public $userId;

    public $mimetype;
    public $path;
    public $isMain;

    public function __construct(string $userId, string $path)
    {
        $this->userId   = $userId;
        $this->mimetype = 'image/png';
        $this->path     = $path;
        $this->isMain   = true;
    }
}
