<?php

namespace Foobar\User\Command;

use Foobar\Common\Command\Command;
use Symfony\Component\Validator\Constraints as Assert;

class CreateUser implements Command
{
    public $id;

    /**
     * @Assert\Email
     */
    public $email;
    public $password;

    public $name;

    /**
     * @Assert\Valid
     */
    public $address;

    /**
     * @Assert\Valid
     */
    public $photos;
}
