<?php

namespace Foobar\User\Command;

use Foobar\Common\Command\Command;

class RemovePendingUser implements Command
{
    public $email;

    public static function byEmail(string $email)
    {
        $command = new static;

        $command->email = $email;

        return $command;
    }
}
