<?php

namespace Foobar\User\Command;

use Foobar\Common\Command\Command;
use Foobar\User\Entity\User;
use Symfony\Component\Validator\Constraints as Assert;

class UpdateUser implements Command
{
    public $id;

    /**
     * @Assert\Email
     */
    public $email;
    public $password;

    public $name;

    /**
     * @Assert\Valid
     */
    public $address;

    /**
     * @Assert\Valid
     */
    public $photos;

    public static function fromState(array $state)
    {
        $command = new static();

        $command->id       = $state['id'];
        $command->email    = $state['email'];
        $command->password = $state['password'];

        $command->name    = $state['name'];
        $command->address = $state['address']->getState();

        return $command;
    }
}
