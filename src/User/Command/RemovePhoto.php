<?php

namespace Foobar\User\Command;

use Foobar\Common\Command\Command;

class RemovePhoto implements Command
{
    public $userId;
    public $path;

    public function __construct(string $userId, string $path)
    {
        $this->userId = $userId;
        $this->path   = $path;
    }
}
