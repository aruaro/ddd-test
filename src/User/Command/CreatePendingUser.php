<?php

namespace Foobar\User\Command;

use Foobar\Common\Command\Command;

class CreatePendingUser implements Command
{
    public $email;
    public $password;

    public function __construct(string $email, string $password)
    {
        $this->email    = $email;
        $this->password = $password;
    }
}
