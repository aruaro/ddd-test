<?php

namespace Foobar\User\Command;

use Foobar\Common\Command\Command;

class RemoveUser implements Command
{
    public $id;

    public static function byId(string $id)
    {
        $command = new static;

        $command->id = $id;

        return $command;
    }
}
