<?php

namespace Foobar\User\Exception;

use Foobar\Common\Exception\AppException;
use Foobar\Common\Exception\ExceptionCode;

class PasswordResetNotFound extends \RuntimeException implements AppException, UserException
{
    public static function fromResetHash(string $resetHash)
    {
        return new static('Password reset hash not found: ' . $resetHash, ExceptionCode::NOTFOUND);
    }
    public static function fromEmptyUser()
    {
        return new static('User is required to reset password.', ExceptionCode::NOTFOUND);
    }
}
