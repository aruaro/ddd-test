<?php

namespace Foobar\User\Exception;

use Foobar\Common\Exception\AppException;
use Foobar\Common\Exception\ExceptionCode;

class UserAlreadyExists extends \RuntimeException implements AppException, UserException
{
    public static function withEmail(string $email)
    {
        return new self('User exists with email: ' . $email, ExceptionCode::ERROR);
    }
}
