<?php

namespace Foobar\User\Exception;

use Foobar\Common\Exception\AppException;
use Foobar\Common\Exception\ExceptionCode;

class UserNotFound extends \RuntimeException implements AppException, UserException
{
    public static function withColumn(string $column, string $value)
    {
        return new self(
            sprintf('User not found with %s: %s', $column, $value),
            ExceptionCode::NOTFOUND
        );
    }
}
