<?php

namespace Foobar\User\Entity;

use Foobar\Common\Entity\Aggregate;
use Foobar\Common\ValueObject\Uuid;
use Foobar\Common\ValueObject\File;

class Photo extends Aggregate
{
    private $userId;
    private $file;
    private $isMain;

    public static function create(string $userId, string $path, string $mimetype)
    {
        $photo = new static();

        $photo->userId = new Uuid($userId);
        $photo->file   = new File($path, $mimetype);
        $photo->isMain = false;

        return $photo;
    }

    public static function fromState(array $state)
    {
        $photo = new static();

        $photo->userId = new Uuid($state['user_id']);
        $photo->file   = new File($state['path'], $state['mimetype']);
        $photo->isMain = $state['is_main'];

        return $photo;
    }

    public function getState() : array
    {
        return [
            'userId'   => $this->getUserId(),
            'path'     => $this->getPath(),
            'mimetype' => $this->getMimetype(),
            'is_main'  => $this->isMain(),
        ];
    }

    public function getUserId(): ?string
    {
        return $this->userId->getUuid();
    }

    public function getPath(): ?string
    {
        return $this->file->getPath();
    }

    public function getMimetype(): ?string
    {
        return $this->file->getMimetype();
    }

    public function isMain() : bool
    {
        return $this->isMain;
    }

    public function setToMain()
    {
        $this->isMain = true;
    }

    public function unsetFromMain()
    {
        $this->isMain = false;
    }
}
