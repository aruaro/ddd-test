<?php

namespace Foobar\User\Entity;

use Foobar\Common\Entity\Aggregate;
use Foobar\Common\ValueObject\Uuid;
use Foobar\User\ValueObject\SecureHash;

class PendingUser extends Aggregate
{
    const EXPIRES_AT = '+30 days';

    private $email;
    private $password;
    private $token;
    private $checked;
    private $expiresAt;

    private function __construct()
    {
        //
    }

    public static function create(string $email, string $password, string $expiresAt = self::EXPIRES_AT)
    {
        $dateInterval = \DateInterval::createFromdateString($expiresAt);

        $pendingUser = new static();

        $pendingUser->email     = $email;
        $pendingUser->password  = $password;
        $pendingUser->checked   = false;
        $pendingUser->token     = SecureHash::generate();
        $pendingUser->expiresAt = (new \DateTimeImmutable())->add($dateInterval);

        return $pendingUser;
    }

    public static function fromState(array $state)
    {
        $pendingUser = new static();

        $pendingUser->email     = $state['email'];
        $pendingUser->password  = $state['password'];
        $pendingUser->token     = new SecureHash($state['token']);
        $pendingUser->checked   = (bool) $state['checked'];
        $pendingUser->expiresAt = new \DateTimeImmutable($state['expires_at']);

        return $pendingUser;
    }

    public function getState() : array
    {
        return [
            'email'      => $this->getEmail(),
            'password'   => $this->getPassword(),
            'token'      => $this->getToken(),
            'checked'    => $this->isChecked(),
            'expires_at' => $this->getExpiresAt(),
        ];
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function getToken(): ?string
    {
        return $this->token->getHash();
    }

    public function getExpiresAt(): ?\DateTimeImmutable
    {
        return $this->expiresAt;
    }

    public function isChecked(): ?bool
    {
        return $this->checked;
    }

    public function markAsChecked()
    {
        $this->checked = true;
    }
}
