<?php

namespace Foobar\User\Entity;

use Foobar\Common\Addressing\ValueObject\Address;
use Foobar\Common\Entity\Aggregate;
use Foobar\Common\ValueObject\Uuid;

class Profile extends Aggregate
{
    private $userId;
    private $name;
    private $address;

    public function __construct()
    {
        $this->address = new Address('', '', '', '', '');
    }

    public static function create(
        string $userId,
        string $name,
        string $street,
        string $city,
        string $locality,
        string $country,
        string $zipcode
    ) {
        $profile = new static();

        $profile->userId  = new Uuid($userId);
        $profile->name    = $name;
        $profile->address = new Address($street, $city, $locality, $country, $zipcode);

        return $profile;
    }

    public static function fromState(array $state)
    {
        $profile = new static();

        $profile->userId  = new Uuid($state['id']);
        $profile->name    = $state['name'];
        $profile->address = Address::fromState($state);

        return $profile;
    }

    public function getState() : array
    {
        return [
            'user_id' => $this->userId,
            'name'    => $this->name,
            'address' => $this->getAddress()->getState(),
        ];
    }
    public function getUserId() : ?string
    {
        return $this->userId ? $this->userId->getUuid() : null;
    }

    public function getName() : ?string
    {
        return $this->name;
    }

    public function getAddress() : Address
    {
        return $this->address;
    }

    public function setUserId(string $userId)
    {
        $this->userId = new Uuid($userId);
    }

    public function update(string $name, Address $address)
    {
        if (!($this->name == $name)) {
            $this->name = $name;
        }

        if (!$this->address->isEqual($address)) {
            $this->address = $address;
        }
    }
}
