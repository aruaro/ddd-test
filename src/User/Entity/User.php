<?php

namespace Foobar\User\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Foobar\Common\Entity\Aggregate;
use Foobar\Common\ValueObject\Email;
use Foobar\Common\ValueObject\Password;
use Foobar\Common\ValueObject\Uuid;
use Foobar\User\ValueObject\Flags;
use Foobar\User\ValueObject\Status;

class User extends Aggregate
{
    private $id;
    private $email;
    private $password;
    private $status;
    private $flags;
    private $createdAt;
    private $updatedAt;

    private $profile;
    private $photos;

    private function __construct()
    {
        $this->status    = Status::initialize();
        $this->flags     = Flags::initialize();
        $this->profile   = new Profile();
        $this->photos    = new ArrayCollection();
        $this->createdAt = new \DateTimeImmutable();
        $this->updatedAt = new \DateTimeImmutable();
    }

    public static function create(string $id, string $email, string $password)
    {
        $user = new static();

        $user->id        = new Uuid($id);
        $user->email     = $email;
        $user->password  = new Password($password);

        $user->profile->setUserId($id);

        return $user;
    }

    public static function fromState(array $state)
    {
        $user = new static();

        $user->id        = new Uuid($state['id']);
        $user->email     = $state['email'] ?? '';
        $user->password  = new Password($state['password'] ?? '');
        $user->status    = new Status($state['status'] ?? '');
        $user->flags     = Flags::fromJson($state['flags'] ?? '{}');
        $user->createdAt = new \DateTimeImmutable($state['created_at']);
        $user->updatedAt = new \DateTimeImmutable($state['updated_at']);

        $user->profile = Profile::fromState($state);

        return $user;
    }

    public function getState() : array
    {
        return [
            'id'         => $this->getId(),
            'email'      => $this->getEmail(),
            'password'   => $this->getPassword(),
            'status'     => $this->getStatus(),
            'flags'      => $this->getFlags(),
            'created_at' => $this->getCreatedAt(),
            'updated_at' => $this->getUpdatedAt(),

            'name'    => $this->getProfile()->getName(),
            'address' => $this->getProfile()->getAddress(),
        ];
    }

    public function getId(): ?string
    {
        return $this->id ? $this->id->getUuid() : null;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function getPassword(): ?string
    {
        return $this->password ? $this->password->getPassword() : null;
    }


    public function getStatus(): ?string
    {
        return $this->status->getStatus();
    }

    public function getFlags(): ?int
    {
        return $this->flags->getFlags();
    }

    public function getCreatedAt(): \DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function getUpdatedAt(): \DateTimeImmutable
    {
        return $this->updatedAt;
    }

    public function getProfile(): Profile
    {
        return $this->profile;
    }

    public function getPhotos(): Collection
    {
        return $this->photos;
    }

    public function changeEmail(string $email)
    {
        if (!($this->email === $email)) {
            $this->email = $email;
        }
    }

    public function changePassword(string $password)
    {
        $password = new Password($password);

        if (!$password->isEmpty() && !$this->password->isEqual($password)) {
            $this->password = $password;
        }
    }

    public function addPhoto(Photo $photo)
    {
        if (!$this->photos->contains($photo)) {
            $this->photos[] = $photo;
        }
    }

    public function removePhoto(Photo $photo)
    {
        if ($this->photos->contains($photo)) {
            $this->photos->removeElement($photo);
        }
    }
}
