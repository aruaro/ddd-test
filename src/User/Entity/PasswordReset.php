<?php

namespace Foobar\User\Entity;

use Foobar\Common\Entity\Aggregate;
use Foobar\Common\ValueObject\Uuid;
use Foobar\User\ValueObject\SecureHash;

class PasswordReset extends Aggregate
{
    const EXPIRES_AT = '+24 hours';

    private $userId;
    private $resetHash;
    private $checked;
    private $expiresAt;

    private function __construct()
    {
        //
    }

    public static function create(string $userId, string $expiresAt = self::EXPIRES_AT)
    {
        $dateInterval = \DateInterval::createFromdateString($expiresAt);

        $passwordReset = new static();

        $passwordReset->userId    = new Uuid($userId);
        $passwordReset->resetHash = SecureHash::generate();
        $passwordReset->checked   = false;
        $passwordReset->expiresAt = (new \DateTimeImmutable())->add($dateInterval);

        return $passwordReset;
    }

    public static function fromState(array $state)
    {
        $passwordReset = new static();

        $passwordReset->userId    = new Uuid($state['user_id']);
        $passwordReset->checked   = (bool) $state['checked'];
        $passwordReset->expiresAt = new \DateTimeImmutable($state['expires_at'] ?? null);

        $passwordReset->resetHash = $state['reset_hash']
            ? new SecureHash($state['reset_hash'])
            : SecureHash::generate();

        return $passwordReset;
    }

    public function getState() : array
    {
        return [
            'user_id'    => $this->getUserId(),
            'reset_hash' => $this->getResetHash(),
            'checked'    => $this->isChecked(),
            'expires_at' => $this->getExpiresAt(),
        ];
    }

    public function getUserId(): ?string
    {
        return $this->userId->getUuid();
    }

    public function getResetHash(): ?string
    {
        return $this->resetHash->getHash();
    }

    public function getExpiresAt(): ?\DateTimeImmutable
    {
        return $this->expiresAt;
    }

    public function isChecked(): ?bool
    {
        return $this->checked;
    }

    public function markChecked()
    {
        $this->checked   = true;
        $this->expiresAt = new \DateTimeImmutable();
    }
}
