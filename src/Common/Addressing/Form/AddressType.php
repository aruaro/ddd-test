<?php

namespace Foobar\Common\Addressing\Form;

use Foobar\Common\Addressing\ValueObject\Address;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\DataMapperInterface;
use Symfony\Component\Form\Extension\Core\Type\CountryType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Webmozart\Assert\Assert;

class AddressType extends AbstractType implements DataMapperInterface
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            // ->setDataMapper($this)
            ->add('street', TextType::class)
            ->add('city', TextType::class)
            ->add('locality', TextType::class)
            ->add('zipcode', TextType::class)
            ->add('country', CountryType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'label'          => false,
            'error_bubbling' => true,
        ]);
    }

    public function getBlockPrefix()
    {
        return '';
    }

    public function mapDataToForms($address, $forms)
    {
        if ($address === null) {
            return;
        }

        Assert::isInstanceOf($address, Address::class);

        $forms = iterator_to_array($forms);

        $forms['street']   = $address->getStreet();
        $forms['city']     = $address->getCity();
        $forms['locality'] = $address->getLocality();
        $forms['country']  = $address->getCountry();
        $forms['zipcode']  = $address->getZipcode();
    }

    public function mapFormsToData($forms, &$data)
    {
        $forms = iterator_to_array($forms);

        $data = new Address(
            $forms['street']->getData(),
            $forms['city']->getData(),
            $forms['locality']->getData(),
            $forms['country']->getData(),
            $forms['zipcode']->getData()
        );
    }
}
