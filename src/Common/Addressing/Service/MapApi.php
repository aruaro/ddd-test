<?php

namespace Foobar\Common\Addressing\Service;

use Foobar\Common\Addressing\ValueObject\Address;
use Foobar\Common\Addressing\ValueObject\Coordinate;

interface MapApi
{
    public function geocodeAddress(Address $address);

    public function createStaticMap(Coordinate $coordinate, array $marker);
}
