<?php

namespace Foobar\Common\Addressing\Service;

use Psr\Log\LoggerInterface;
use GuzzleHttp\Client as GuzzleClient;
use GuzzleHttp\ClientInterface as GuzzleClientInterface;
use GuzzleHttp\Exception\GuzzleException;
use Foobar\Common\Addressing\ValueObject\Address;
use Foobar\Common\Addressing\ValueObject\Coordinate;

class Mapbox
{
    const BASE_URL       = 'https://api.mapbox.com';
    const DEFAULT_MARKER = ['pin' => 'pin-l-star', 'color' => '7435b2'];

    private $guzzle;
    private $logger;

    public function __construct(
        string $mapboxKey,
        ?GuzzleClientInterface $client = null,
        ?LoggerInterface $logger = null
    ) {
        $this->guzzle = $client ?? new GuzzleClient([
            'base_uri' => self::BASE_URL,
            'query'    => [
                'access_token' => $mapboxKey
            ]
        ]);

        $this->logger = $logger;
    }

    public function geocodeAddress(Address $address) : Address
    {
        $place = urlencode($address->getPlaceName() . ' ' . $address->getLongFormat());
        $uri   = '/geocoding/v5/mapbox.places/' . $place . '.json';
        $query = [
            'country'      => $address->getCountry(),
            'language'     => 'en',
            'autocomplete' => true,
            'fuzzyMatch'   => true,
            'limit'        => 1,
            'cachebuster'  => microtime(true) / 1000000 / 60
        ];

        $result = @json_decode($this->send($uri, $query) ?: [], true);

        if (empty($result) || !isset($result['features'][0]['center'])) {
            return $address;
        }

        $addressState = $address->getState();

        $addressState['longitude'] = $result['features'][0]['center'][0];
        $addressState['latitude']  = $result['features'][0]['center'][1];

        return Address::fromState($addressState);
    }

    public function createStaticMap(Coordinate $coordinate, array $marker) : ?string
    {
        // /styles/v1/mapbox/light-v10/static/[pin]+[color]([lng,lat])/[lng,lat],16,0,60/[size]

        $formattedLatLng = $coordinate->getLongitude() . ',' . $coordinate->getLatitude();
        $formattedMarker = sprintf('%s+%s(%s)', $marker['pin'], $marker['color'], $formattedLatLng);

        $uri = sprintf(
            '/styles/v1/symfony/cjuae4e931baj1fntoe9q5v64/static/%s/%s,16,0,50/%s',
            $formattedMarker,
            $formattedLatLng,
            '400x400'
        );

        $result = $this->send($uri);

        return $result ? base64_encode($result) : null;
    }

    private function send(string $uri, array $query = [])
    {
        $query = array_merge($this->guzzle->getConfig('query') ?? [], $query);

        try {
            $response = $this->guzzle->request('GET', $uri, ['query' => $query]);

            if ($response->getStatusCode() === 200) {
                return $response->getBody()->getContents();
            }
        } catch (GuzzleException $exception) {
            if ($this->logger) {
                $this->logger->error($exception->getMessage());
            }
        }

        return false;
    }
}
