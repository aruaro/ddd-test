<?php

namespace Foobar\Common\Addressing\ValueObject;

class Coordinate
{
    protected $latitude;
    protected $longitude;

    public function __construct(float $latitude, float $longitude)
    {
        $this->latitude  = $latitude;
        $this->longitude = $longitude;
    }

    public static function fromState(array $state)
    {
        return new static($state['latitude'], $state['longitude']);
    }

    public function getState(): array
    {
        return [
            'latitude'  => $this->getLatitude(),
            'longitude' => $this->getLongitude(),
        ];
    }

    public function getLatitude(): ?float
    {
        return floatval($this->latitude ?? 0);
    }

    public function getLongitude(): ?float
    {
        return floatval($this->longitude ?? 0);
    }

    public function isEqual(Coordinate $coordinate)
    {
        return $this->getLatitude() == $coordinate->getLatitude()
            && $this->getLongitude() == $coordinate->getLongitude()
        ;
    }
}
