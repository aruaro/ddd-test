<?php

namespace Foobar\Common\Addressing\ValueObject;

use Symfony\Component\Intl\Intl;

class Address
{
    protected $placeName;
    protected $street;
    protected $city;
    protected $locality;
    protected $country;
    protected $zipcode;
    protected $coordinate;

    public function __construct(
        string $street,
        string $city,
        string $locality,
        string $country,
        string $zipcode,
        string $placeName = '',
        ?Coordinate $coordinate = null
    ) {
        $this->street     = $street;
        $this->city       = $city;
        $this->locality   = $locality;
        $this->country    = $country;
        $this->zipcode    = $zipcode;
        $this->placeName  = $placeName;
        $this->coordinate = $coordinate ?? new Coordinate(0, 0);
    }

    public static function fromState(array $state)
    {
        return new static(
            $state['street'],
            $state['city'],
            $state['locality'],
            $state['country'],
            $state['zipcode'],
            $state['place_name'] ?? '',
            new Coordinate(
                $state['latitude'] ?? 0,
                $state['longitude'] ?? 0
            )
        );
    }

    public function getState()
    {
        return [
            'street'     => $this->getStreet(),
            'city'       => $this->getCity(),
            'locality'   => $this->getLocality(),
            'country'    => $this->getCountry(),
            'zipcode'    => $this->getZipcode(),
            'place_name' => $this->getPlaceName(),
            'latitude'   => $this->getCoordinate()->getLatitude(),
            'longitude'  => $this->getCoordinate()->getLongitude(),
        ];
    }

    public function getShortFormat(): string
    {
        return sprintf(
            '%s, %s, %s',
            $this->getCity(),
            $this->getLocality(),
            $this->getCountry()
        );
    }

    public function getLongFormat(): string
    {
        return sprintf(
            '%s %s, %s, %s %s',
            $this->getStreet(),
            $this->getCity(),
            $this->getLocality(),
            $this->getCountry(),
            $this->getZipcode()
        );
    }

    public function getStreet(): string
    {
        return $this->street ?? '';
    }

    public function getCity(): string
    {
        return $this->city ?? '';
    }

    public function getLocality(): string
    {
        return $this->locality ?? '';
    }

    public function getCountry(): string
    {
        return $this->country ?? '';
    }

    public function getCountryName(): string
    {
        return Intl::getRegionBundle()->getCountryName($this->country)
            ?: $this->country;
    }

    public function getZipcode(): string
    {
        return $this->zipcode ?? '';
    }

    public function getPlaceName() : string
    {
        return $this->placeName ?? '';
    }

    public function getCoordinate(): Coordinate
    {
        return $this->coordinate;
    }

    public function addPlaceName(string $placeName) : Address
    {
        $state = $this->getState();

        $state['place_name'] = $placeName;

        return static::fromState($state);
    }

    public function updateCoordinate(Coordinate $coordinate) : Address
    {
        $state = $this->getState();

        if (!$this->getCoordinate()->isEqual($coordinate)) {
            $state['latitude']  = $coordinate->getLatitude();
            $state['longitude'] = $coordinate->getLongitude();
        }

        return static::fromState($state);
    }

    public function isEqual(Address $address)
    {
        return $this->getStreet() == $address->getStreet()
            && $this->getCity() == $address->getCity()
            && $this->getLocality() == $address->getLocality()
            && $this->getCountry() == $address->getCountry()
            && $this->getZipcode() == $address->getZipcode()
            && $this->getCoordinate()->isEqual($address->getCoordinate())
        ;
    }
}
