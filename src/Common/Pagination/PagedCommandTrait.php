<?php

namespace Foobar\Common\Pagination;

trait PagedCommandTrait
{
    public $page  = 1;
    public $limit = 20;

    public $sortField;
    public $sortOrder;

    public static function byPage(int $page, int $limit)
    {
        $command = new static();

        $command->page  = $page;
        $command->limit = $limit;

        return $command;
    }

    public function sort(string $sortField, string $sortOrder = 'asc')
    {
        $this->sortField = $sortField;
        $this->sortOrder = $sortOrder;
    }
}
