<?php

namespace Foobar\Common\Pagination;

use Doctrine\DBAL\Query\QueryBuilder;
use Knp\Component\Pager\Event\ItemsEvent;
use Knp\Component\Pager\Event\PaginationEvent;
use Knp\Component\Pager\Pagination\SlidingPagination;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class DbalPaginationSubscriber implements EventSubscriberInterface
{
    public static function getSubscribedEvents()
    {
        return [
            'knp_pager.items'      => 'paginate',
            'knp_pager.pagination' => 'setPagination'
        ];
    }

    public function paginate(ItemsEvent $event)
    {
        if (!($event->target instanceof QueryBuilder)) {
            return;
        }

        $event->count =  $this->countResults($event->target);

        $event->items = [];

        if ($event->count) {
            $queryBuilder = clone $event->target;

            $queryBuilder
                ->setFirstResult($event->getOffset())
                ->setMaxResults($event->getLimit())
            ;

            $event->items = $this->fetchResults($queryBuilder, $event->options);

            if (isset($event->options['dataTransformer'])) {
                unset($event->options['dataTransformer']);
            }
        }

        $event->stopPropagation();
    }

    public function setPagination(PaginationEvent $event)
    {
        $event->setPagination(new SlidingPagination);
        $event->stopPropagation();
    }

    private function fetchResults(QueryBuilder $queryBuilder, array $pagerOptions)
    {
        $sortField       = $pagerOptions['defaultSortFieldName'] ?? null;
        $sortOrder       = $pagerOptions['defaultSortDirection'] ?? null;
        $dataTransformer = $pagerOptions['dataTransformer'] ?? null;

        if ($sortField && $sortOrder) {
            $queryBuilder->addOrderBy($sortField, $sortOrder);
        }

        if ($dataTransformer != null && !is_callable($dataTransformer)) {
            throw new \InvalidArgumentException(
                'Pagination result transformer must be callable or null'
            );
        }

        return array_map($dataTransformer, $queryBuilder->execute()->fetchAll());
    }

    private function countResults(QueryBuilder $origQueryBuilder)
    {
        $queryBuilder = clone $origQueryBuilder;

        $queryBuilder->resetQueryPart('orderBy');

        $sql = $queryBuilder->getSQL();

        $queryBuilder
            ->resetQueryParts()
            ->select('COUNT(*) as cnt')
            ->from('(' . $sql . ')', 'dbal_count_tbl')
        ;

        return $queryBuilder->execute()->fetchColumn(0);
    }
}
