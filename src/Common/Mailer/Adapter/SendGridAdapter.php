<?php

namespace Foobar\Common\Mailer\Adapter;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use Psr\Http\Message\ResponseInterface;
use Foobar\Common\Mailer\Message\Message;

class SendGridAdapter implements MailerAdapter
{
    const BASE_URI = 'https://api.sendgrid.com';
    const SEND_URI = '/v3/mail/send';

    private $guzzle;
    private $sandboxMode;

    public function __construct(string $apiKey, bool $sandboxMode = false)
    {
        $this->guzzle = new Client([
            'base_uri' => self::BASE_URI,
            'headers' => [
                'Authorization' => 'Bearer ' . $apiKey,
                'Content-Type'  => 'application/json'
            ]
        ]);

        $this->sandboxMode = $sandboxMode;
    }

    public function send(Message $message)
    {
        $postData = [
            'subject' => $message->subject(),
            'from'    => current($message->senders()),
            'content' => [],
            'personalizations' => [[
                'to' => array_values($message->receivers()),
            ]],
            'mail_settings' => [
                'sandbox_mode' => [
                    'enable' => $this->sandboxMode
                ]
            ]
        ];

        if ($message->htmlBody()) {
            $postData['content'][] = [
                'type'  => 'text/html',
                'value' => $message->htmlBody()
            ];
        }

        if ($message->plainBody()) {
            $postData['content'][] = [
                'type'  => 'text/plain',
                'value' => $message->plainBody()
            ];
        }

        $this->guzzle->post(self::SEND_URI, ['json' => $postData]);
    }
}
