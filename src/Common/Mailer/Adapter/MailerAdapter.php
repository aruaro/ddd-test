<?php

namespace Foobar\Common\Mailer\Adapter;

use Foobar\Common\Mailer\Message\Message;

interface MailerAdapter
{
    public function send(Message $message);
}
