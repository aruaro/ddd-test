<?php

namespace Foobar\Common\Mailer\Adapter;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use Psr\Http\Message\ResponseInterface;
use Foobar\Common\Mailer\Message\Message;

class MailgunAdapter implements MailerAdapter
{
    const BASE_URI = 'https://api.mailgun.net';
    const SEND_URI = '/v3/mg.symfony.app/messages';

    private $guzzle;
    private $sandboxMode;

    public function __construct(string $mailgunKey, bool $sandboxMode = false)
    {
        $this->guzzle = new Client([
            'base_uri' => self::BASE_URI,
            'auth'     => ['api', $mailgunKey],
            'headers'  => [
                'Content-Type' => 'application/x-www-form-urlencoded'
            ]
        ]);

        $this->sandboxMode = $sandboxMode;
    }

    public function send(Message $message)
    {
        $postData = [
            'subject' => $message->subject(),
            'from'    => $this->formatAddresses($message->senders()),
            'to'      => $this->formatAddresses($message->receivers())
        ];

        if ($message->plainBody()) {
            $postData['text'] = $message->plainBody();
        }

        if ($message->htmlBody()) {
            $postData['html'] = $message->htmlBody();
        }

        $this->guzzle->post(self::SEND_URI, ['form_params' => $postData]);
    }

    private function formatAddresses(array $addresses)
    {
        $formatted = [];

        foreach ($addresses as $address) {
            $formatted[] = sprintf('%s <%s>', $address['name'], $address['email']);
        }

        return implode(', ', $formatted);
    }
}
