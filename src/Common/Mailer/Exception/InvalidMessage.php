<?php

namespace Foobar\Common\Mailer\Exception;

use Foobar\Common\Exception\AppException;
use Foobar\Common\Exception\ExceptionCode;

class InvalidMessage extends \LogicException implements AppException, MailerException
{
    public static function fromEmptySender()
    {
        return new self('Composer does not contain any sender.', ExceptionCode::WARNING);
    }

    public static function fromEmptyReceiver()
    {
        return new self('Composer does not contain any receiver.', ExceptionCode::WARNING);
    }

    public static function fromEmptySubject()
    {
        return new self('Composer does not contain any subject.', ExceptionCode::WARNING);
    }

    public static function fromEmptyBody()
    {
        return new self('Composer does not contain any body.', ExceptionCode::WARNING);
    }
}
