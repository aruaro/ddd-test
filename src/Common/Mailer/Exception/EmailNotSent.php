<?php

namespace Foobar\Common\Mailer\Exception;

use Foobar\Common\Exception\AppException;
use Foobar\Common\Exception\ExceptionCode;

class EmailNotSent extends \RuntimeException implements AppException, MailerException
{
    public function __construct(\Exception $previous = null)
    {
        parent::__construct(
            $previous ? $previous->getMessage() : 'Email not sent.',
            ExceptionCode::ERROR
        );
    }
}
