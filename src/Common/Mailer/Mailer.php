<?php

namespace Foobar\Common\Mailer;

use Foobar\Common\Mailer\Message\Message;

interface Mailer
{
    public function send(Message $message);
}
