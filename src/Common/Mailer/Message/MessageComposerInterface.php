<?php

namespace Foobar\Common\Mailer\Message;

interface MessageComposerInterface
{
    public function compose(array $params) : Message;
}
