<?php

namespace Foobar\Common\Mailer\Message;

abstract class MessageComposer
{
    protected $subject  = '';
    protected $template = '';

    public function compose(array $params) : Message
    {
        $sender   = $params['sender'];
        $receiver = $params['receiver'];
        $body     = $this->getBody($params['data'] ?? []);

        $message = new Message($this->subject, $body);
        $message->addSender($sender['name'], $sender['email']);
        $message->addReceiver($receiver['name'], $receiver['email']);

        return $message;
    }

    protected function getBody(array $data) : string
    {
        return '';
    }
}
