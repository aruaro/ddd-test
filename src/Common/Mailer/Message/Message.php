<?php

namespace Foobar\Common\Mailer\Message;

class Message
{
    private $senders   = [];
    private $receivers = [];
    private $subject;
    private $htmlBody;
    private $plainBody;

    public function __construct(string $subject, string $htmlBody)
    {
        $this->subject  = $subject;
        $this->htmlBody = $htmlBody;
    }

    public function senders() : array
    {
        return $this->senders;
    }

    public function receivers() : array
    {
        return $this->receivers;
    }

    public function subject() : ?string
    {
        return $this->subject;
    }

    public function htmlBody() : ?string
    {
        return $this->htmlBody;
    }

    public function plainBody() : ?string
    {
        return $this->plainBody ?? '';
    }

    public function addSender(string $name, string $email)
    {
        $this->senders[$email] = [
            'name'  => $name,
            'email' => $email,
        ];
    }

    public function addReceiver(string $name, string $email)
    {
        $this->receivers[$email] = [
            'name'  => $name,
            'email' => $email,
        ];
    }

    public function addPlainBody(string $body)
    {
        $this->plainBody = $body;
    }
}
