<?php

namespace Foobar\Common\Mailer;

use Foobar\Common\Mailer\Adapter\MailerAdapter;
use Foobar\Common\Mailer\Exception\EmailNotSent;
use Foobar\Common\Mailer\Exception\InvalidMessage;
use Foobar\Common\Mailer\Message\Message;

class SimpleMailer implements Mailer
{
    /** @var MailerAdapter */
    protected $adapter;

    public function __construct(MailerAdapter $adapter)
    {
        $this->adapter = $adapter;
    }

    public function send(Message $message)
    {
        $this->validateMessage($message);

        try {
            $this->adapter->send($message);
        } catch (\Exception $exception) {
            throw new EmailNotSent($exception);
        }
    }

    private function validateMessage(Message $message)
    {
        if (!$message->senders()) {
            throw InvalidMessage::fromEmptySender();
        }

        if (!$message->receivers()) {
            throw InvalidMessage::fromEmptyReceiver();
        }

        if (!$message->subject()) {
            throw InvalidMessage::fromEmptySubject();
        }

        if (!$message->htmlBody()) {
            throw InvalidMessage::fromEmptyBody();
        }
    }
}
