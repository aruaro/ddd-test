<?php

namespace Foobar\Common\Search\Exception;

use Foobar\Common\Exception\AppException;
use Foobar\Common\Exception\ExceptionCode;

class InvalidSearchCriteria extends \LogicException implements AppException, SearchException
{
    public static function fromEmpty()
    {
        return new self('No search criteria has been set.', ExceptionCode::WARNING);
    }

    public static function fromNullQueryBuilder()
    {
        return new self('Cannot build search criteria without QueryBuilder.', ExceptionCode::ERROR);
    }
}
