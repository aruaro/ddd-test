<?php

namespace Foobar\Common\Search\Criteria;

abstract class SearchCriteria
{
    protected $field;
    protected $value;

    public function getField()
    {
        return $this->field;
    }

    public function getValue()
    {
        return $this->value;
    }

    abstract public function getExpression();
}
