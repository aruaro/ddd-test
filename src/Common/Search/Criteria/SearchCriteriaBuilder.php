<?php

namespace Foobar\Common\Search\Criteria;

use Doctrine\Common\Collections\ArrayCollection;

abstract class SearchCriteriaBuilder
{
    /** @var ArrayCollection */
    protected $criteriaList = [];

    public function add(SearchCriteria $criteria)
    {
        $this->criteriaList[] = $criteria;

        return $this;
    }

    abstract public function build();
}
