<?php

namespace Foobar\Common\Search\Criteria;

use Doctrine\DBAL\Query\QueryBuilder;
use Foobar\Common\Search\Exception\InvalidSearchCriteria;
use Doctrine\DBAL\Query\Expression\CompositeExpression;

class DbalSearchCriteriaBuilder extends SearchCriteriaBuilder
{
    /** @var QueryBuilder */
    private $queryBuilder;

    public function getQueryBuilder() : QueryBuilder
    {
        return $this->queryBuilder;
    }

    public function setQueryBuilder(QueryBuilder $queryBuilder)
    {
        $this->queryBuilder = $queryBuilder;
    }

    public function build()
    {
        $this->validateOnBuild();

        $expressionGroup = new CompositeExpression(CompositeExpression::TYPE_OR);

        foreach ($this->criteriaList as $criteria) {
            if (!($criteria->getField() && $criteria->getValue())) {
                continue;
            }

            $expressionGroup->add($criteria->getExpression());

            $this->queryBuilder->setParameter(':' . $criteria->getField(), $criteria->getValue());
        }

        if ($expressionGroup->count()) {
            $this->queryBuilder->where($expressionGroup);
        }
    }

    private function validateOnBuild()
    {
        if (!($this->queryBuilder instanceof QueryBuilder)) {
            throw InvalidSearchCriteria::fromNullQueryBuilder();
        }

        if ($this->queryBuilder->getState() == QueryBuilder::STATE_CLEAN) {
            throw new InvalidSearchCriteria('QueryBuilder has not been initiated');
        }

        if ($this->queryBuilder->getType() != QueryBuilder::SELECT) {
            throw new InvalidSearchCriteria('QueryBuilder is not a SELECT statement');
        }
    }
}
