<?php

namespace Foobar\Common\Exception;

class NotImplemented extends \RuntimeException implements AppException
{
    public function __construct(string $className, string $methodName)
    {
        parent::__construct(
            sprintf('Not implemented: %s::%s()', $className, $methodName),
            ExceptionCode::ERROR
        );
    }
}
