<?php

namespace Foobar\Common\Exception;

use Foobar\Common\Exception\AppException;
use Symfony\Component\EventDispatcher\Event;

class EventNotProjected extends \RuntimeException implements AppException
{
    public function __construct(Event $event, \Exception $previous = null)
    {
        parent::__construct(
            'Event not projected: ' . get_class($event),
            ExceptionCode::ERROR,
            $previous
        );
    }
}
