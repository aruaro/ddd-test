<?php

namespace Foobar\Common\Exception;

use Symfony\Component\Debug\ExceptionHandler as BaseExceptionHandler;
use Symfony\Component\Debug\Exception\FlattenException;
use Symfony\Component\HttpFoundation\Response;

class ExceptionHandler extends BaseExceptionHandler
{
    private $debug;

    public function __construct(bool $debug = true, string $charset = null, $fileLinkFormat = null)
    {
        parent::__construct($debug, $charset, $fileLinkFormat);

        $this->debug = $debug;
    }

    public function sendPhpResponse($exception)
    {
        if (!$exception instanceof FlattenException) {
            $exception = FlattenException::create($exception);
        }

        if (!headers_sent()) {
            header(sprintf('HTTP/1.0 %s', $exception->getStatusCode()));
            foreach ($exception->getHeaders() as $name => $value) {
                header($name.': '.$value, false);
            }
            header('Content-Type: text/html; charset=utf-8');
        }

        print $this->renderErrorPage($exception);
    }

    public function getHtml($exception)
    {
        if (!$exception instanceof FlattenException) {
            $exception = FlattenException::create($exception);
        }

        return $this->renderErrorPage($exception);
    }

    private function renderErrorPage(FlattenException $exception)
    {
        $templateDir = $_SERVER['DOCUMENT_ROOT'] . '/../templates/bundles/TwigBundle/Exception';

        switch ($exception->getStatusCode()) {
            case 404:
                $template = file_get_contents($templateDir . '/error404.html.twig');
                break;
            default:
                $template = file_get_contents($templateDir. '/error.html.twig');
        }

        if (!$this->debug) {
            return str_replace('<!-- STACKTRACE -->', '<!-- PHP_STACKTRACE -->', $template);
        }

        $content = [];

        foreach ($exception->toArray() as $position => $e) {
            $content[] = "\n" . str_repeat('.', 70) . "\n";

            $content = array_merge(
                $content,
                [$e['message']. "\n"],
                $this->formatTrace($e)
            );

            foreach ($e['trace'] as $trace) {
                $content = array_merge($content, $this->formatTrace($trace), ['']);
            }
        }

        return str_replace('<!-- STACKTRACE -->', implode("\n", $content), $template);
    }

    private function formatTrace(array $trace)
    {
        $content = [];

        $content[] = sprintf(
            '%s%s%s',
            $trace['class'] ?? '',
            $trace['type'] ?? '',
            isset($trace['function']) && !empty($trace['function']) ? $trace['function'] . '()' : ''
        );

        if (($trace['file'] ?? false) || ($trace['line'] ?? false)) {
            $content[] = sprintf(
                '%s::%s',
                $trace['file'] ?? '(unknown)',
                $trace['line'] ?? '(unknown)'
            );
        }

        return array_filter($content);
    }
}
