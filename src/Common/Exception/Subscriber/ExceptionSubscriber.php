<?php

namespace Foobar\Common\Exception\Subscriber;

use Foobar\Common\Exception\AppException;
use Foobar\Common\Exception\ExceptionCode;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\HttpFoundation\RedirectResponse;

class ExceptionSubscriber implements EventSubscriberInterface
{
    public static function getSubscribedEvents()
    {
        return [
          KernelEvents::EXCEPTION => [
                ['processNotFound', 10],
                ['processFailedCommand', -10],
          ]
        ];
    }

    public function processNotFound(GetResponseForExceptionEvent $event)
    {
        $exception = $event->getException();

        switch ($exception->getCode()) {
            case ExceptionCode::NOTFOUND:
                throw new NotFoundHttpException($exception->getMessage(), $exception);
        }
    }

    public function processFailedCommand(GetResponseForExceptionEvent $event)
    {
        $request = $event->getRequest();

        if ($request->getMethod() == 'GET') {
            return;
        }

        $request->getSession()->getFlashBag()->add(
            'alert.error',
            $request->attributes->get('_error_msg', 'Failed to perform action.')
        );

        $request->getSession()->getFlashBag()->add(
            'alert.error',
            $event->getException()->getMessage()
        );

        $event->setResponse(new RedirectResponse(
            $request->attributes->get('_fallback_route', '/')
        ));
    }
}
