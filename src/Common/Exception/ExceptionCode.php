<?php

namespace Foobar\Common\Exception;

class ExceptionCode
{
    const ERROR    = 500;
    const NOTFOUND = 400;
    const WARNING  = 100;
}
