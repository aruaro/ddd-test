<?php

namespace Foobar\Common\ValueObject;

class File
{
    protected $path;
    protected $mimetype;

    public function __construct(string $path, string $mimetype)
    {
        $this->path     = $path;
        $this->mimetype = $mimetype;
    }

    public static function fromState(array $state)
    {
        return new static($state['path'], $state['mimetype']);
    }

    public static function fromUrl(string $url)
    {
        return new static($url, 'image/jpeg');
    }

    public function getState()
    {
        return [
            'path'     => $this->path,
            'mimetype' => $this->mimetype,
        ];
    }

    public function getPath(): ?string
    {
        return $this->path;
    }

    public function getMimetype(): ?string
    {
        return $this->mimetype;
    }
}
