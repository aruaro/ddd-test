<?php

namespace Foobar\Common\ValueObject;

class Photo extends File
{
    protected $allowedTypes = [
        'image/gif',
        'image/png',
        'image/jpeg',
        'image/bmp',
        'image/webp',
    ];
}
