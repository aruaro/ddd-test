<?php

namespace Foobar\Common\ValueObject;

class Email
{
    protected $email;

    public function __construct($email)
    {
        $this->email = $email;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function isEqual(Email $email)
    {
        return $this->getEmail() === $email->getEmail();
    }
}
