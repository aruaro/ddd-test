<?php

namespace Foobar\Common\ValueObject;

use Ramsey\Uuid\Uuid as BaseUuid;

class Uuid
{
    private $uuid;

    public function __construct($uuid = null)
    {
        $this->uuid = null === $uuid ? BaseUuid::uuid4()->toString() : $uuid;
    }

    public function __toString()
    {
        return $this->uuid;
    }

    public function getUuid()
    {
        return $this->uuid;
    }

    public function isEqual(Uuid $uuid)
    {
        return $this->getUuid() === $uuid->getUuid();
    }
}
