<?php

namespace Foobar\Common\ValueObject;

class Password
{
    private $password;

    public function __construct($password)
    {
        $this->password = $password;
    }

    public function getPassword()
    {
        return $this->password;
    }

    public function isEmpty() : bool
    {
        return empty($this->getPassword());
    }

    public function isEqual(Password $password)
    {
        return $this->getPassword() === $password->getPassword();
    }
}
