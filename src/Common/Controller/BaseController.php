<?php

namespace Foobar\Common\Controller;

use Foobar\Common\Command\Command;
use Foobar\Common\Command\CommandBus;
use Foobar\Common\ValueObject\Uuid;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

abstract class BaseController extends AbstractController
{
    protected $commandBus;

    public function __construct(CommandBus $commandBus)
    {
        $this->commandBus = $commandBus;
    }

    protected function createId()
    {
        return (new Uuid())->getUuid();
    }
}
