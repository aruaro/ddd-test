<?php

namespace Foobar\Common\Security;

use Symfony\Component\Security\Core\User\UserInterface;
use Webmozart\Assert\Assert;

final class SecurityUser implements UserInterface
{
    private $id;
    private $login;
    private $password;
    private $roles;

    public function __construct(string $id, string $login, string $password, array $roles = [])
    {
        Assert::stringNotEmpty($id);
        Assert::stringNotEmpty($login);

        $this->id       = $id;
        $this->login    = $login;
        $this->password = $password;
        $this->roles    = $roles;
    }

    public function __toString()
    {
        return $this->getId();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getLogin()
    {
        return $this->login;
    }

    public function getUsername()
    {
        return $this->getId();
    }

    public function getPassword()
    {
        return $this->password;
    }

    public function getRoles()
    {
        return $this->roles;
    }

    public function getSalt()
    {
        //
    }

    public function eraseCredentials()
    {
        //
    }
}
