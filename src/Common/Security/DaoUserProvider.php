<?php

namespace Foobar\Common\Security;

use Foobar\User\Repository\UserRepository;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\Role\Role;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;

class DaoUserProvider implements UserProviderInterface
{
    /** @var UserRepository */
    private $repository;

    public function __construct(UserRepository $repository)
    {
        $this->repository = $repository;
    }

    public function loadUserByUsername($login) : UserInterface
    {
        return $this->createSecurityUser(
            $this->repository->findByLogin($login ?? '')
        );
    }

    public function refreshUser(UserInterface $user) : UserInterface
    {
        if (!$user instanceof SecurityUser) {
            throw new UnsupportedUserException(get_class($user) . ' is not a proper User class.');
        }

        return $this->createSecurityUser(
            $this->repository->findByLogin($user->getLogin(), $user->getId()) ?: null
        );
    }

    public function supportsClass($class) : bool
    {
        return SecurityUser::class === $class;
    }

    private function createSecurityUser($user) : UserInterface
    {
        if (!$user) {
            throw new UsernameNotFoundException();
        }

        return new SecurityUser($user['id'], $user['login'], $user['password'], ['ROLE_USER']);
    }
}
