<?php

namespace Foobar\Common\Command;

use Symfony\Component\EventDispatcher\Event;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

final class EventDispatchingCommandBus implements CommandBus
{
    private $commandBus;
    private $eventDispatcher;

    public function __construct(
        SimpleCommandBus $commandBus,
        ?EventDispatcherInterface $eventDispatcher = null
    ) {
        $this->commandBus      = $commandBus;
        $this->eventDispatcher = $eventDispatcher ?? new EventDispatcher();

        foreach ($this->commandBus->getHandlers() as $handler) {
            if ($handler instanceof EventDispatchingCommandHandler) {
                $handler->setEventDispatcher($this->eventDispatcher);
            }
        }
    }

    public function getHandlers() : array
    {
        return $this->commandBus->getHandlers();
    }

    public function subscribe(CommandHandler $handler) : bool
    {
        if ($handler instanceof EventDispatchingCommandHandler) {
            $handler->setEventDispatcher($this->eventDispatcher);
        }

        return $this->commandBus->subscribe($handler);
    }

    public function dispatch(Command $command)
    {
        return $this->commandBus->dispatch($command);
    }
}
