<?php

namespace Foobar\Common\Command;

interface CommandBus
{
    public function getHandlers();

    public function subscribe(CommandHandler $handler);

    public function dispatch(Command $command);
}
