<?php

namespace Foobar\Common\Command\Exception;

use Foobar\Common\Exception\AppException;
use Foobar\Common\Exception\ExceptionCode;

class InvalidCommand extends \InvalidArgumentException implements AppException, CommandException
{
    public function __construct(string $command, string $message = '')
    {
        parent::__construct(
            sprintf('Command invalid: %s (%s)' . $command, $message),
            ExceptionCode::ERROR
        );
    }
}
