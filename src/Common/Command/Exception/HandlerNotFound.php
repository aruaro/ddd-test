<?php

namespace Foobar\Common\Command\Exception;

use Foobar\Common\Exception\AppException;
use Foobar\Common\Exception\ExceptionCode;

class HandlerNotFound extends \RuntimeException implements AppException, CommandException
{
    public function __construct(string $command)
    {
        parent::__construct('Handler not found for command: ' . $command, ExceptionCode::ERROR);
    }
}
