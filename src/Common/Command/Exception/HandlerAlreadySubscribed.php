<?php

namespace Foobar\Common\Command\Exception;

use Foobar\Common\Exception\AppException;
use Foobar\Common\Exception\ExceptionCode;

class HandlerAlreadySubscribed extends \RuntimeException implements AppException, CommandException
{
    public function __construct(string $handlerClass)
    {
        parent::__construct('Handler already subscribed: ' . $handlerClass, ExceptionCode::ERROR);
    }
}
