<?php

namespace Foobar\Common\Command;

interface CommandHandler
{
    public function handle(Command $command);
}
