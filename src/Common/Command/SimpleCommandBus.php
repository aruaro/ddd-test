<?php

namespace Foobar\Common\Command;

use Foobar\Common\Command\Exception\HandlerNotFound;
use Foobar\Common\Command\Exception\HandlerAlreadySubscribed;

final class SimpleCommandBus implements CommandBus
{
    private $handlers = [];
    private $handlerFound;
    private $isDispatching;

    public function __construct(iterable $handlers = [])
    {
        $this->handlerFound  = false;
        $this->isDispatching = false;

        foreach ($handlers as $handler) {
            $this->subscribe($handler);
        }
    }

    public function getHandlers() : array
    {
        return $this->handlers;
    }

    public function isDispatching() : bool
    {
        return $this->isDispatching;
    }

    public function subscribe(CommandHandler $handler) : bool
    {
        $handlerClass = get_class($handler);

        if (array_key_exists($handlerClass, $this->handlers)) {
            throw new HandlerAlreadySubscribed($handlerClass);
        }

        $this->handlers[$handlerClass] = $handler;

        return true;
    }

    public function dispatch(Command $command)
    {
        $output = null;

        if (!$this->isDispatching) {
            $this->isDispatching = true;

            try {
                $output = $this->callHandler($command);

                $this->isDispatching = false;
            } catch (\Exception $exception) {
                $this->isDispatching = false;

                throw $exception;
            }

            return $output;
        }
    }

    private function callHandler(Command $command)
    {
        foreach ($this->handlers as $handler) {
            try {
                return $handler->handle($command);
            } catch (\BadMethodCallException $exception) {
                $handler->resetCalledState();
            } catch (\Exception $exception) {
                $handler->resetCalledState();

                throw $exception;
            }
        }

        throw new HandlerNotFound(get_class($command));
    }
}
