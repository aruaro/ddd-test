<?php

namespace Foobar\Common\Command;

use Symfony\Component\EventDispatcher\Event;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Webmozart\Assert\Assert;

abstract class EventDispatchingCommandHandler extends SimpleCommandHandler
{
    protected $eventDispatcher;

    public function setEventDispatcher(EventDispatcherInterface $eventDispatcher)
    {
        $this->eventDispatcher = $eventDispatcher;
    }

    public function handle(Command $command)
    {
        return parent::handle($command);
    }

    protected function dispatchEvent(Event $event)
    {
        Assert::isInstanceOf($this->eventDispatcher, EventDispatcherInterface::class);

        $this->eventDispatcher->dispatch($event::NAME, $event);
    }
}
