<?php

namespace Foobar\Common\Command;

abstract class SimpleCommandHandler implements CommandHandler
{
    protected $isHandlerCalled = false;

    public function isHandlerCalled() : bool
    {
        return $this->isHandlerCalled;
    }

    public function resetCalledState()
    {
        $this->isHandlerCalled = false;
    }

    public function handle(Command $command)
    {
        $method = $this->getHandleMethod($command);
        $output = $this->$method($command);

        $this->isHandlerCalled = true;

        return $output;
    }

    private function getHandleMethod(Command $command)
    {
        $classParts = explode('\\', get_class($command));
        $method     = 'handle'.end($classParts);

        $this->isCommandSupported($command, $method);

        return $method;
    }

    private function isCommandSupported(Command $command, string $method)
    {
        if (method_exists($this, $method)) {
            $reflection  = new \ReflectionMethod($this, $method);
            $methodParam = $reflection->getParameters()[0];

            if ($methodParam->getType()->getName() == get_class($command)) {
                return;
            }
        }

        throw new \BadMethodCallException(
            'Method not found: ' . get_class($this) . ':' . $method
        );
    }

    protected function invalidCommand(Command $command, string $message = '')
    {
        throw new Exception\InvalidCommand(get_class($command), $message);
    }
}
