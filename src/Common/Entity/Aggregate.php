<?php

namespace Foobar\Common\Entity;

abstract class Aggregate
{
    public static function fromState(array $state)
    {
        return new static();
    }

    public function getState() : array
    {
        return [];
    }
}
